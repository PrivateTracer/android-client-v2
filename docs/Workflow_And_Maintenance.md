# Workflow & maintenance

## Environments
Develop with the `devDebug` buildType by default. For more information how to handle the different environments in the app, see [Environments](./Environments.md)  



## Code style
The project includes some `.idea` files like [codeStyles](https://gitlab.com/PrivateTracer/android-client-v2/-/tree/develop/.idea/codeStyles)
to maintain the same code styling throughout the project.

*Highly recommended*  
- Either use the Android Studio's VCS commit tool and turn on `reformat code` and `optimize imports` `before commit`
- Or make sure to turn on the `format on save` setting in Android Studio.

##### Format on save
Create a macro that includes the actions `ReformatCode`, `OptimizeImports` and `SaveAll` on a Ctrl-S hotkey combo to easily maintain
code formatting at all time!
Start with `Edit > Macros > Start Macro Recording`, followed by all hotkeys for the actions above, and finally `Stop Macro Recording`



## Automated testing
TODO



## Continuous Integration
TODO



## Source control
Source control is maintained using git in [Gitlab](https://gitlab.com/PrivateTracer/androidclient)

TODO: decisions have yet to be made to align branching & reviewing workflow. Perhaps [this](https://nvie.com/posts/a-successful-git-branching-model/) git branching model could be used as starting point.



## Releases
TODO



## Tasks and issues
All open tasks & issues can be found on the [issue board](https://gitlab.com/PrivateTracer/android-client-v2/-/boards)  
Not all issues are placed on the issue board of this project, but some transcend several projects / disciplines.  
All issues related to Android can be found using [this link](https://gitlab.com/groups/PrivateTracer/-/boards/1685871?&label_name[]=Android)

Check [this](https://gitlab.com/PrivateTracer/coordination/-/blob/master/Onboarding%20-%20Getting%20to%20Know%20PrivateTracer/Gitlab%20issue%20board.md) document to get a rough idea how the board is used.
