# Support and Accessibility
All decisions and implications regarding device support (app availability in PlayStore) and accessibility are documented here.

## Support
With device support we either mean if the current design takes a specific feature into account, or whether the app is downloadable from the PlayStore for that specific device.
The latter depends on several things, including: device size, minimum SDK version and required features

#### Device size
We decided to ignore tablet design support, until demand is high enough (only after initial release).  
We do not exclude specific device sizes from the PlayStore though!

#### minSdkVersion
The higher the minimum SDK version, the less devices are supported (app showing up in the PlayStore).
For this project, a SDK 18+ is necessary for [Bluetooth LE](https://developer.android.com/guide/topics/connectivity/bluetooth-le) (receiving) and SDK 21+ for [BluetoothLeAdvertiser](https://developer.android.com/reference/android/bluetooth/le/BluetoothLeAdvertiser) (sending), so for the complete interaction usecase we would need SDK 21.  

#### Required features
Whether a device shows up in the PlayStore also depends on the required features, which are defined in the [AndroidManifest](../app/src/main/AndroidManifest.xml)
For this project Bluetooth LE is considered to be the only required feature.

#### Support implications
We currently restrict device support below SDK 21 and devices without Bluetooth LE due to reasons described above.

In March 2020, this would be the distribution of Android (SDK) versions for all (active) devices in The Netherlands. (`total %` is the percentage of devices excluded if that specific SDK would be set as minimumSDK)

| SDK  | Android version | device % | total % |
| ---- | --------------- | ---------| ------- |
| 21   | 5.0             | 0.92     | 1.61    |
| 18   | 4.3             | 0.05     | 0.37    |
| 14   | 4.0             | 0.02     | 0.07    |

[Source](https://gs.statcounter.com/android-version-market-share/mobile-tablet/netherlands)
...
[How were these numbers calculated?](https://gs.statcounter.com/faq#mobile-definition)

This means that for the currently suggested minimum SDK of 21, we'd exclude approximately 1.6% of all (active) mobile Android devices,
which is a significant amount considering the amount of possible users.

*Notes:*
- This percentage is an estimation of the number of devices excluded from the PlayStore: it's not easy to assess the exact amount.
- Probably not all the devices we dó support will own bluetooth hardware itself, so these will still be unsupported despite the minimum SDK version.
- These unsupported devices will partially consist of tablets (which are not our main concern right now).

##### Suggestions
If we'd like to support more devices, we would need a different alternative to have an app with minimum required functionality (interaction tracing).
An alternative method to support interaction tracing would be [WiFi Direct](https://developer.android.com/training/connect-devices-wirelessly/wifi-direct), which is added in SDK 14.
However, we haven't explored the possibilities and restrictions of this technique yet.

Another alternative would be to make bluetooth not a required feature and lower the minimum SDK so many more devices would be supported *without* a minimum interaction usecase.  
The app would not function with its primary feature, but it would allow us to explain the user why the app doesn't work for this specific device.
This could for example be shown in a popup when the app is started.

##### Decisions
The WiFi Direct alternative is yet unexplored, so it's possibly not a realistic short term solution to support more users.  
Both downloading a not-working app, and the app not showing up in the PlayStore are not optimal situations either: both would probably raise questions or frustration for certain users.  
We have yet to decide which solution is the 'lesser of evils', and how to properly communicate this to ao. users and the government.

TODO:
- which is the lowest minimum SDK we want to support?
- should bluetooth be a required feature, or will we support WiFi direct?
- what are the reasons for, and implications of these decisions?
- how do we properly communicate to users/government which devices are not supported and why?
- do we want a downloadable app for as many users as possible, with an 'unsupported device' popup at app startup?
- are these decisions final, or just for the initial release?



## Accessibility
These are the accessibility concerns currently taken into account.

| Feature              | Implemented | Suggestion   | Decisions |
| -------------------- | ----------- | ------------ | ----------|
| Language             | Dutch       |              | Dutch & English at 1st release. And make sure all hardcoded text is extracted to translatable strings files |
| Colorblind friendly  | ?           |              | ?         |
| Scalable text        | NO          | enabled (sp) | should not make the design look horrible. All elements still visible and clickable |
| Text contrast        | NO          | >= 4.5       | same?     |
| Image contrast       | NO          | >= 3.0       | same?     |
| Clickable areas      | NO          | >= 48dp W/H  | same      |
| Content descriptions | YES         | spoken aloud | no warnings 1st release, afterwards make it more user friendly <sup>1</sup> |

*Note:*
- Implemented means it was fully implemented at some point, and it can of course be outdated. Make sure we maintain accessibility support for this!
- The suggestions are either the advice by Android and/or noted by the [accessibility scanner](https://support.google.com/accessibility/android/answer/6376570).
- TODO: not all the decisions are made yet.

1. Screen reading apps should be able to read all features and elements in the app aloud.  
Preferably this shouldn't just be properly understandable, but a smooth story (long term feature!).  
Note that not all images need a description, some are not functional but purely decorative.
Advice can be found [here](https://material.io/design/usability/accessibility.html#imagery).

