Bark Mode
=========

An idea based on the following news article:  
https://www.nrc.nl/nieuws/2020/04/25/coronavirus-een-veel-beter-idee-de-blafapp-a3997840

The idea is that your phone starts barking (or in this case vibrating) as soon as, and as long as, another PrivateTracer user is nearby.
  
Not only will this help/remind people to keep distance, it'll also help with calibration.  
As now we have a clear signal, when another device has been detected.  

BarkMode can only be activated on debug-builds for now.
There is a toggle-switch in the SettingsBottomSheet.
It's also possible to change the sensitivity to further help with calibration.
Changing the sensitivity of the BarkMode *does not* affect the sensitivity of the regular tracking.
The tracking sensitivity can only be changed by hardcoding or downloadable configs.  
