Certificate Pinning
===================

To prevent MITM attacks, we use certificate pinning to verify the authenticity of the server.

When connecting to with HTTPS, you can receive a certificate from the server, this will contain a public key,
we check if this public key is included in our list of verified public keys, that is included inside the app.

How it works
------------
We use Retrofit with OkHttp. OkHttp has support for certificate pinning.
First we create a `CertificatePinner` like so
```Kotlin
val certificatePinner = CertificatePinner.Builder()
.add("*.example.com","sha256/AAAAAAAAAAAAAAA")
.build()
```
Then we add it to the `OkHttpClient`
```Kotlin
val client = OkHttpClient.Builder()
.certificatePinner(certificatePinner)
.build()
```
And finally we add the `OkHttpClient` to `Retrofit`
```Retrofit.Builder()
.client(client)
.baseUrl("https://example.com")
.build()
.create(IExampleService::class.java)
```
In the first step, we've set the `sha256`, this is obivously wrong and that's intentional.
We now run the app, and look at the exception that is thrown, should look like:
```
javax.net.ssl.SSLPeerUnverifiedException: Certificate pinning failure!
      Peer certificate chain:
        sha256/FllMKi0vHGZutOKupiVKHRJ2odJ6Lk6r73XvRscyrY0=: CN=*.example.com
        sha256/RCbqB+W8nwjznTeP4O6VjqcwdxIgI79eBpnBKRr32gc=: CN=Example Corp. IT TLS CA 5,OU=IT,O=Example Corporation
        sha256/Y9mvm0exBk1JoQ57f9Vm28jKo5lFm/woKcVxrYxu80o=: CN=ExampleTrust Root,OU=ExampleTrust
      Pinned certificates for example.example.com:
        sha256/AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
        at okhttp3.CertificatePinner.check$okhttp(CertificatePinner.kt:192)
```
From this exception, we can copy and paste the actual sha256, and put it in the app.

References used
---------------
- https://medium.com/@appmattus/android-security-ssl-pinning-1db8acb6621e
- https://square.github.io/okhttp/https/#certificate-pinning-kt-java
- https://square.github.io/okhttp/4.x/okhttp/okhttp3/-certificate-pinner/