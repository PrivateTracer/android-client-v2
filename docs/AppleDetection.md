# Detecting iOS Devices

Because of security implications on the iOS platform, we have to do a
specific set of steps to retrieve EphemeralIDs from iOS. These steps
might no longer be necessary after Apple releases their Covid tracking
API, but for now we have the following workaround.

Compared to Android, iOS advertises a `Service` (not `ServiceData`!)
with a certain `ServiceUUID`. By convention, this allows clients to
connect to the device and to subsequently discover the advertised
service.

The high level steps are as follows:

- Detect a BLE device advertising a service with the correct UUID, but
  no data
- Connect to the BLE device using Gatt.
- Look for a service offered over the connection that matches the same
  UUID as earlier
- Look for a list of all characteristics; there should only be one,
  and its (random) UUID is the EphemeralID encoded as a UUID

> Note: We do not have to actually /read/ the characteristic, as its
> 'name' is all we are interested in.

Unlike earlier attempts at making the Android <--> iOS communications
work, there is currently no use for iBeacons.

An open challenge is still that the approach outlined here does not
work consistently on all Android devices. Please open an issue
describing your setup (phone model, Android version, any relevant
logcat logs) if you run into this situation.

