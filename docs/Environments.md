# Environments
The app environment consists of 2 main configurations: buildTypes and productFlavors.
More dimensions can be added in the future by adding an additional productFlavor dimension
(these can for example be used for different brands, or free vs paid versions).
The current situation is as follows:

### BuildTypes
- Release is used for production purposes only.
- Acceptance closely resembles the release app. The main difference is that it's not signed.
- Debug is used to develop with. ***Try to use this BuildConfig as much as possible***.
Compared to Release and Acceptance, Debug builds faster (no ProGuard obfuscation & optimization) and is debuggable (incl. logs).

### ProductFlavors
Flavors can have several dimensions. We're currently using only a single dimension:
The `environment` flavorDimension is used for different backend urls, which include:
- Prod is used to call the production backend (at moment of writing this does not yet exist)
- Demo is used to call the demo backend
- Dev is used to call the development backend. ***Try to use this ProductFlavor as much as possible***.
- Local can be configured to call a local environment (2)

### Which configuration should I use?
Currently there are 3 buildTypes & 4 productFlavors: so 3 x 4 = 12 theoretical config combinations.
Rest assured, you don't need to use all of these!  
Use the following cases as rule of thumb:  

- The main config to remember is `devDebug`, which can be used to develop on. This is also the default config.
- When setting up a local environment to develop on, you'd go with `localDebug`.
- When giving a demo, the `demoAcceptance` would be the best config to use.
- When you're (acceptance) testing the app just before a release, the `devAcceptance` or `demoAcceptance` would be most useful (1)
- The released app will be built from `prodRelease`. This config is meant for the PlayStore only!
- If you need to debug or test something specifically for the demo or production backend, then use `demoDebug` or `prodDebug`.  
***However, always try to avoid using the production environment, and only work on it if absolutely necessary!***

Figuring out the correct configuration based on the differences between buildTypes and productFlavors should usually not be too hard.  
When in doubt, discuss with other developers what would be the most logical to do.



###### (1) Important note for testing:
Testing on Debug is usually fine, this is nearly the same as testing the app on a Release or Acceptance buildType.
However, ProGuard obfuscation can cause issues unnoticeable on Debug:  
Obfuscation makes classes 'unreadable', resulting in a crash if these are used for reflection.
This is the case for eg. service definitions and request/response models used by Retrofit and native methods from the dp3t library.  
To be on the safe side, always test on the `acceptance` buildType (which also performs ProGuard obfuscation) before releasing,
especially when new models were added, or existing ones were moved out of the packages excluded by ProGuard-rules.

###### (2) Local environment:
First setup the local environment as described [here](https://gitlab.com/PrivateTracer/server.azure#supporting-local-mobile-app-development).  
This local environment can be easily used when the productFlavor `Local` is used and the `BASE_URL_LOCAL` value is overridden in the global `gradle.properties`.
By overriding the global `gradle.properties` each developer can configure a custom IP address without changing the project's [`gradle.properties`](../gradle.properties)
The [Signing docs](Signing.md) describe how to override the global `gradle.properties` in more detail.
