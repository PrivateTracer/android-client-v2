# Signing

## Debug

The Debug BuildConfig is signed with a `Debug.keystore` saved in this project (so all developers will use the same one).
This prevents other developers form having to re-install the app if running/updating it on the same device.

## Release

Release builds are signed using a `release.keystore`. Due to security reasons, the keystore in this project is a dummy. Signing release can be done as follows:


1. Add the `release.keystore` file somewhere locally, for example in your project's [keystores](../keystores) directory.
2. Copy the signing properties from the [`gradle.properties`](../gradle.properties) file and add these to your local (global) `gradle.properties` file (`USER_HOME/.gradle`).
These local `gradle.properties` will then override the project's [`gradle.properties`](../gradle.properties).
3. Change the `signing.store.file` path of your local `gradle.properties` to the location where the keystore from step 1 was added, for example `../keystores/private_tracer.keystore`.
4. Also change the values of the alias and passwords to the correct values.

## Notes:
- Passwords and the `release.keystore` can be requested from other developers, or found on the build server (TODO).
- The global `gradle.properties` file is located in the `USER_HOME/.gradle` directory, not in this project, because the
location of the `keystore.properties` can be different for each developer.
- Files in the [keystores](../keystores) directory - like the `release.keystore` - are already excluded from source control by GitIgnore.
Do not add the keystore or passwords in another directory to prevent is from being saved in source control!

