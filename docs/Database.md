# Database

### Inspect local database
To inspect the data in the local (mobile) database, you can use the [Android-Debug-Database](https://github.com/amitshekhariitbhu/Android-Debug-Database) tool
Filter logs for tag 'DebugDB' to see where to find the database on physical device.
On emulator, run `adb forward tcp:8080 tcp:8080` and go to <http://localhost:8080>
