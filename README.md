# AndroidClient
The project that contains the Android version of the client that is used by the Private Tracer application.



## Setup
- git clone the project & open in [Android Studio](https://developer.android.com/studio)
- In `Tools -> SDK Manager -> SDK Tools`, install the necessary Android SDK (Platform) Tools (and accept the license(s))
  - a [`devDebug`-build](./docs/Environments.md) can be run in Android Studio, which displays clear error messages which necessary tools aren't installed
  - The [Environments docs](./docs/Environments.md) explain how to use other environments, and how to setup a local environment.
- After all installations are finished, restart Android Studio and build the project
  - If the compiler complains about not being able to find ADB, then add platform-tools to your `$PATH`
    - This should be `/Users/<you>/Library/Android/platform-tools`
- You will either need a physical device (and connect it using USB debugging), or setup an Android Virtual Device (AVD) to run this project.

##### Connect a device
- The device should be set in developer mode:
  - `Settings -> System -> About phone (under Advanced) -> Build number`.
  - Hit the build number text 7 times
- USB debugging should be turned on:
  - `Settings -> System -> Developer Options -> USB Debugging`
- Now you should see your device in Android Studio when connecting it via USB.

##### Release signing
Release builds require signing - so some additional manual setup - which is described [here](./docs/Signing.md)



## Documentation
Important info about the project's workflow and maintenance (like code styling, automated testing and releases) are described
[here](./docs/Workflow_And_Maintenance.md).  
All other documentation under source control can be found [here](./docs)
