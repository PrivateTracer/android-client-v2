package org.privatetracer.api.interceptors

import android.util.Log
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import org.privatetracer.api.errors.RetryLimitException
import java.net.SocketTimeoutException

class RetryApiCallInterceptor : Interceptor {

    companion object {
        private const val MAX_TRY_COUNT = 6
        private const val TAG = "RetryApiCallInterceptor"
    }

    private fun shouldRetry(tryCount: Int, response: Response?): Boolean {
        val isResponseSuccessful = response?.isSuccessful ?: false
        val isWithinTryCount = tryCount < MAX_TRY_COUNT
        return (!isResponseSuccessful && isWithinTryCount)
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        val request: Request = chain.request()
        var response: Response? = null
        var tryCount = 0
        do {
            try {
                response = chain.proceed(request)
            } catch (e: SocketTimeoutException) {
                Log.w(TAG, "Timeout connecting with try attempt $tryCount / $MAX_TRY_COUNT")
            } finally {
                tryCount++
            }
        } while (shouldRetry(tryCount, response))
        if (null == response) {
            throw RetryLimitException("No proper response after $tryCount / $MAX_TRY_COUNT tries")
        }
        return response
    }
}
