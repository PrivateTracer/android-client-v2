package org.privatetracer.api.interceptors

import okhttp3.Interceptor
import okhttp3.Response
import org.privatetracer.api.errors.NoInternetConnection
import org.privatetracer.api.errors.NoNetworkConnection
import org.privatetracer.api.utils.NetworkConnectionHandler

/**
 * [Interceptor] to check the network connection during an call to the server
 * Based on [https://medium.com/@elye.project/android-intercept-on-no-internet-connection-acb91d305357]
 */
class NetworkConnectionInterceptor(
    private val networkConnectionHandler: NetworkConnectionHandler
) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        return checkNetworkConnection { chain.proceedRequest() }
    }

    private fun checkNetworkConnection(throwable: Throwable? = null, onConnected: () -> Response): Response {
        if (!networkConnectionHandler.isConnectionOn()) {
            throw NoNetworkConnection(throwable)
        } else if (!networkConnectionHandler.isInternetAvailable()) {
            throw NoInternetConnection(throwable)
        } else {
            return onConnected()
        }
    }

    /**
     * Execute the network call and return the response.
     *
     * When the network connection is lost during a call, we cannot reliably determine this from the response:
     * The call could fail during different steps, each resulting in a different exception or failed response.
     * The most reliable way is to directly check the connection again.
     * If we're not connected anymore, then we can assume this is caused by an unstable connection,
     * as we've validated the connection before we started the call as well.
     */
    private fun Interceptor.Chain.proceedRequest(): Response {
        return try {
            proceed(request()).let { response ->
                if (!response.isSuccessful) {
                    checkNetworkConnection {
                        response
                    }
                } else {
                    response
                }
            }
        } catch (e: Exception) {
            checkNetworkConnection(e) { throw e }
        }
    }
}
