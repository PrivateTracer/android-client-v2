package org.privatetracer.api.utils

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import androidx.annotation.RequiresApi
import java.io.IOException
import java.net.InetSocketAddress
import java.net.Socket

/**
 * Checks network state.
 * Based on [https://medium.com/@elye.project/android-intercept-on-no-internet-connection-acb91d305357]
 */
interface NetworkConnectionHandler {
    /** Checks if WiFi or Mobile Data network is active */
    fun isConnectionOn(): Boolean

    /** Checks if internet is available by making a quick ping to the Google DNS */
    fun isInternetAvailable(): Boolean

    /** Checks both [isConnectionOn] and [isInternetAvailable]: a full internet connection check! */
    fun isConnectedToInternet(): Boolean
}

class DefaultNetworkConnectionHandler(
    context: Application
) : NetworkConnectionHandler {
    companion object {
        private const val INTERNET_CHECK_TIMEOUT_MILLIS = 1500
        private val GOOGLE_DNS = InetSocketAddress("8.8.8.8", 53)
    }

    private val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

    override fun isConnectionOn() = connectivityManager.let {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            it.postAndroidMInternetCheck()
        } else {
            it.preAndroidMInternetCheck()
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun ConnectivityManager.postAndroidMInternetCheck(): Boolean {
        return getNetworkCapabilities(activeNetwork)?.let {
            it.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) || it.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)
        } ?: false
    }

    @Suppress("DEPRECATION")
    private fun ConnectivityManager.preAndroidMInternetCheck(): Boolean {
        return activeNetworkInfo?.let {
            it.type == ConnectivityManager.TYPE_WIFI || it.type == ConnectivityManager.TYPE_MOBILE
        } ?: false
    }

    override fun isInternetAvailable(): Boolean {
        return try {
            Socket().use {
                it.connect(GOOGLE_DNS, INTERNET_CHECK_TIMEOUT_MILLIS)
            }
            true
        } catch (e: IOException) {
            false
        }
    }

    override fun isConnectedToInternet() = isConnectionOn() && isInternetAvailable()
}
