package org.privatetracer.api.models

import com.google.gson.annotations.SerializedName

data class RivmAdvice(
    @SerializedName("id")
    val id: String,
    @SerializedName("isolationPeriodDays")
    val isolationPeriodDays: Int,
    @SerializedName("observedTracingKeyRetentionDays")
    val observedTracingKeyRetentionDays: Int,
    @SerializedName("text")
    val text: List<Text>,
    @SerializedName("tracingKeyRetentionDays")
    val tracingKeyRetentionDays: Int
)
