package org.privatetracer.api.models

import com.google.gson.annotations.SerializedName

data class Configuration(
    @SerializedName("RivmAdvice")
    val rivmAdvice: String,
    @SerializedName("TracingSets")
    val tracingSet: TracingSetConfig
)
