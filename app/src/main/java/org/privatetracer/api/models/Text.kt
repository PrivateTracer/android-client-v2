package org.privatetracer.api.models

import com.google.gson.annotations.SerializedName

data class Text(
    @SerializedName("isolationAdviceLong")
    val isolationAdviceLong: String,
    @SerializedName("isolationAdviceShort")
    val isolationAdviceShort: String,
    @SerializedName("locale")
    val locale: String
)
