package org.privatetracer.api.models

import com.google.gson.annotations.SerializedName

data class TracingSet(
    @SerializedName("content")
    val content: String,
    @SerializedName("id")
    val id: String
)
