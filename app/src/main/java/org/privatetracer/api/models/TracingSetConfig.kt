package org.privatetracer.api.models

import com.google.gson.annotations.SerializedName

data class TracingSetConfig(
    @SerializedName("Ids")
    val ids: List<String>
)
