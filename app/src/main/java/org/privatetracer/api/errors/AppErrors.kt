package org.privatetracer.api.errors

import java.io.IOException

abstract class AppError(message: String, cause: Throwable? = null) : IOException(message, cause)

class RetryLimitException(message: String) : AppError(message)

/** Eg. when WiFi and mobile data are turned off, no mobile service, network is disconnected / re-connecting */
class NoNetworkConnection(cause: Throwable? = null) : AppError("No network available", cause)

/** Eg. when connected to public WiFi but haven't accepted the conditions of that network yet. */
class NoInternetConnection(cause: Throwable? = null) : AppError("No internet available", cause)
