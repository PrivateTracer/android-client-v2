package org.privatetracer

import android.app.Application
import android.content.Context
import android.os.Vibrator
import org.privatetracer.api.TracerApi
import org.privatetracer.api.interceptors.NetworkConnectionInterceptor
import org.privatetracer.api.utils.DefaultNetworkConnectionHandler
import org.privatetracer.contact.BarkModeEstimator
import org.privatetracer.contact.ContactManager
import org.privatetracer.contact.MemoryContactRepo
import org.privatetracer.contact.RssiEstimator
import org.privatetracer.dp3t.Dp3t
import org.privatetracer.storage.DataRetentionManager
import org.privatetracer.storage.EpochDayCalculator
import org.privatetracer.storage.PrivateTracerDatabase
import org.privatetracer.storage.PrivateTracerPreferences

class PrivateTracerApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        Dp3t.init()

        // Singleton inits
        TracerApi.initialize(NetworkConnectionInterceptor(DefaultNetworkConnectionHandler(this)))
        PrivateTracerDatabase.initialize(this)
        val prefs = PrivateTracerPreferences(this)
        ContactManager.initialize(
            PrivateTracerDatabase.instance.contactDao(),
            MemoryContactRepo,
            DataRetentionManager(prefs),
            EpochDayCalculator(),
            RssiEstimator(),
            BarkModeEstimator(this),
            getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
        )
    }
}
