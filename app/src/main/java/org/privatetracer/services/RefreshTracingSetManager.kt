package org.privatetracer.services

import android.app.Service
import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.work.*
import org.privatetracer.ConfigurableConstants.Companion.REFRESH_TRACING_SETS_MINUTES
import java.util.concurrent.TimeUnit

/**
 * A background worker job to refresh TracingSets
 * This enables the device to spread its memory resources and battery consumption.
 * If this isn't implemented using the [WorkManager] (but eg. a background [Service]), devices running newer
 * Android versions might decide to kill the background process!
 * Read more [here](https://medium.com/@suchibansal/android-workmanager-bfc0fd3dd9a2)
 */
class RefreshTracingSetManager(context: Context, workerParams: WorkerParameters) : Worker(context, workerParams) {
    companion object {
        private const val TAG = "RefreshTracinSetManager"
        const val REFRESH_TRACING_SETS = "org.privatetracer.REFRESH_TRACING_SETS"

        private val workManager = WorkManager.getInstance()

        /**
         * Refreshing TracingSets is triggered and repeated periodically in the background.
         * The background work can be delayed if the device is busy or idle, but only by a maximum amount of time.
         * If work is already being performed, it is cancelled before a new work is started to prevent a buildup.
         */
        fun start() {
            stop()
            workManager.enqueue(
                periodicRequestBuilder
                    .setConstraints(constraints)
                    .addTag(TAG)
                    .build()
            )
        }

        fun stop() {
            workManager.cancelAllWorkByTag(TAG)
        }

        // Use a short periodic interval to easily test the background work on DEBUG.
        // This is also meant to prevent issues with testing below MIN_PERIODIC_INTERVAL_MILLIS,
        // which could take a while to notice.
        private val periodicRequestBuilder =
            PeriodicWorkRequest.Builder(
                RefreshTracingSetManager::class.java, REFRESH_TRACING_SETS_MINUTES, TimeUnit.MINUTES
            )

        // TODO #28: maybe NetworkType.NOT_ROAMING or .UNMETERED is a better choice? https://gitlab.com/PrivateTracer/android-client-v2/-/issues/28
        //   In that case setTriggerContentMaxDelay could be very high, so that it waits long enough for a suitable network connection.
        //   For example: wait for a NOT_ROAMING connection for a maximum of 12 hours, otherwise execute this on a ROAMING network nonetheless.
        //   ps. we might make a different choice for dev or demo environments as well?
        private val constraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .setTriggerContentMaxDelay(30, TimeUnit.MINUTES)
            .build()
    }

    override fun doWork(): Result {
        refreshTracingSets()
        return Result.success()
    }

    private fun refreshTracingSets() {
        Log.d(TAG, "refreshTracingSets")
        val intent = Intent().apply { action = REFRESH_TRACING_SETS }
        applicationContext.sendBroadcast(intent)
    }
}
