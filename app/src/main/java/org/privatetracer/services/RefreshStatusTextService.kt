package org.privatetracer.services

import android.os.Handler
import android.util.Log
import java.util.concurrent.TimeUnit

class RefreshStatusTextRunnable : Runnable {
    companion object {
        const val TAG = "RefreshStatusTxtService"
    }

    private lateinit var callback: () -> Unit
    private var isRunning: Boolean = false

    @Suppress("PrivatePropertyName")
    private val REFRESH_INTERVAL = TimeUnit.MINUTES.toMillis(15)

    private val handler = Handler()

    fun start(callback: () -> Unit) {
        if (!isRunning) {
            isRunning = true
            this.callback = callback
            run()
        }
    }

    override fun run() {
        if (isRunning) {
            Log.d(TAG, "update the refresh status text")
            callback.invoke()
            handler.postDelayed(this, REFRESH_INTERVAL)
        }
    }

    fun stop() {
        Log.d(TAG, "removing post refreshStatusTextRunnable from handler")
        handler.removeCallbacks(this)
        isRunning = false
    }
}
