package org.privatetracer.view.customviews

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.util.AttributeSet
import android.view.View

class CircularProgressView constructor(context: Context, attrs: AttributeSet? = null) :
    View(context, attrs) {

    var progress = 75
    var max = 100
    private val strokeW = 25F
    private var rect = RectF()
    private val paint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        color = Color.rgb(255, 46, 46)
        style = Paint.Style.STROKE
        strokeWidth = strokeW
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val w = getDefaultSize(suggestedMinimumWidth, widthMeasureSpec)
        val h = getDefaultSize(suggestedMinimumHeight, heightMeasureSpec)
        val min = w.coerceAtMost(h)
        setMeasuredDimension(min, min)
        rect.set(
            (strokeW / 2),
            (strokeW / 2),
            min - (strokeW / 2),
            min - (strokeW / 2)
        )
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        val angle = 360 * progress / max
        canvas?.drawArc(rect, -90F, angle.toFloat(), false, paint)
    }
}
