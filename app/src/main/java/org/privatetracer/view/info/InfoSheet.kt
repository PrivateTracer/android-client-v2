package org.privatetracer.view.info

import android.view.View
import kotlinx.android.synthetic.main.sheet_info.view.*
import org.privatetracer.R
import org.privatetracer.view.BaseBottomSheet

class InfoSheet : BaseBottomSheet() {

    override val layoutResourceId: Int
        get() = R.layout.sheet_info

    override fun initUI(rootView: View) {
        rootView.infoCloseBtn.setOnClickListener {
            dismiss()
        }
    }
}
