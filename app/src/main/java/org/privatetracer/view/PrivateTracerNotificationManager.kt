package org.privatetracer.view

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.os.Build
import androidx.core.app.NotificationManagerCompat
import org.privatetracer.view.splash.SplashActivity

// This is an overview of all notification(channel) IDs to prevent duplicates
const val CHANNEL_ID_TRACING = "CHANNEL_ID_TRACING"
const val CHANNEL_ID_STATUS_AT_RISK = "CHANNEL_ID_STATUS_AT_RISK"
const val CHANNEL_ID_STATUS_SAFE = "CHANNEL_ID_STATUS_SAFE"

const val NOTIFICATION_ID_TRACING = 101
const val NOTIFICATION_ID_INFECTION_STATUS = 102

abstract class PrivateTracerNotificationManager(context: Context) : ContextWrapper(context) {
    protected fun showNotification(notificationId: Int, notification: Notification) {
        NotificationManagerCompat.from(this).notify(notificationId, notification)
    }

    /** Clicking notification should resume the app where we left off: https://stackoverflow.com/a/42002705/2660216 */
    protected fun getPendingIntentResumeApp(): PendingIntent {
        val notificationIntent = packageManager.getLaunchIntentForPackage(packageName)?.apply {
            setPackage(null)
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED
        } ?: run {
            // If a launchIntent could not be created, then simply start the SplashActivity
            Intent(this, SplashActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            }
        }
        return PendingIntent.getActivity(this, 0, notificationIntent, 0)
    }

    protected fun createNotificationChannel(channel: NotificationChannel) {
        if (shouldCreateNotificationChannel()) {
            getSystemService(NotificationManager::class.java)?.createNotificationChannel(channel)
        }
    }

    protected fun shouldCreateNotificationChannel() = Build.VERSION.SDK_INT >= Build.VERSION_CODES.O
}
