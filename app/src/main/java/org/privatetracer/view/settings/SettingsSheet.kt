package org.privatetracer.view.settings

import android.app.ActivityManager
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.View
import android.widget.SeekBar
import android.widget.Switch
import android.widget.TextView
import kotlinx.android.synthetic.main.sheet_settings.view.*
import org.privatetracer.BuildConfig
import org.privatetracer.ConfigurableConstants.Companion.REFRESH_TRACING_SETS_MINUTES
import org.privatetracer.R
import org.privatetracer.contact.BarkModeEstimator
import org.privatetracer.extensions.show
import org.privatetracer.extensions.toInterface
import org.privatetracer.storage.PrivateTracerPreferences
import org.privatetracer.view.BaseBottomSheet
import org.privatetracer.view.main.TracingStatusServicesRunner
import java.util.concurrent.TimeUnit

class SettingsSheet : BaseBottomSheet() {

    override val layoutResourceId: Int
        get() = R.layout.sheet_settings

    //Bark mode
    private lateinit var settingsBarkModeSwitch: Switch
    private lateinit var settingsBarkModeDesc2: TextView
    private lateinit var settingsBarkModeSeekBar: SeekBar

    private lateinit var tracingStatusServicesRunner: TracingStatusServicesRunner

    override fun onAttach(context: Context) {
        super.onAttach(context)
        tracingStatusServicesRunner = context.toInterface()
    }

    override fun initUI(rootView: View) {
        val settingsAppVersionTV = rootView.settingsAppVersionTV
        settingsAppVersionTV.text = getString(R.string.app_version_text).format(BuildConfig.VERSION_NAME)

        val settingsDeleteAllBtn = rootView.settingsDeleteAllBtn
        settingsDeleteAllBtn.setOnClickListener {
            AlertDialog.Builder(rootView.context).apply {
                setMessage(getString(R.string.delete_are_you_sure))
                setPositiveButton(android.R.string.yes) { _, _ -> deleteAllData(); dismiss() }
                setNegativeButton(android.R.string.no) { _, _ -> dismiss() }
                show()
            }
        }

        val prefs = PrivateTracerPreferences(requireContext())

        rootView.settingsAutoMatchDesc.text =
            String.format(getString(R.string.auto_match_desc), TimeUnit.MINUTES.toHours(REFRESH_TRACING_SETS_MINUTES))
        val settingsAutoMatchSwitch = rootView.settingsAutoMatchSwitch
        settingsAutoMatchSwitch.isChecked = prefs.isAutoMatchEnabled()
        settingsAutoMatchSwitch.setOnCheckedChangeListener { _, isChecked ->
            prefs.setAutoMatchEnabled(isChecked)
            onAutoMatchToggled(isChecked)
        }

        val settingsCloseBtn = rootView.settingsCloseBtn
        settingsCloseBtn.setOnClickListener {
            dismiss()
        }

        rootView.rivm_advices_button.setOnClickListener {
            navigateToRivmAdvicesWebPage()
        }

        initUIBarkMode(rootView, prefs)
    }

    private fun onAutoMatchToggled(isAutoMatchTurnedOn: Boolean) {
        if (isAutoMatchTurnedOn) {
            tracingStatusServicesRunner.startTracingStatusOnBackground()
        } else {
            tracingStatusServicesRunner.stopTracingStatusOnBackground()
        }
    }

    private fun initUIBarkMode(rootView: View, prefs: PrivateTracerPreferences) {
        if (!BuildConfig.DEBUG) return //TODO: remove this check when feature is done

        rootView.bark_mode_container.show()

        settingsBarkModeSwitch = rootView.settingsBarkModeSwitch
        settingsBarkModeSeekBar = rootView.settingsBarkModeSeekBar
        settingsBarkModeDesc2 = rootView.settingsBarkModeDesc2

        settingsBarkModeSwitch.isChecked = prefs.isBarkModeEnabled()
        settingsBarkModeSwitch.setOnCheckedChangeListener { _, isChecked ->
            prefs.setBarkModeEnabled(isChecked)
            updateUIBarkMode(prefs)
        }
        settingsBarkModeSeekBar.setOnSeekBarChangeListener(object :
            SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                if (fromUser) {
                    prefs.setBarkModeSensitivity(progress)
                    updateUIBarkMode(prefs)
                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {}
            override fun onStopTrackingTouch(seekBar: SeekBar?) {}
        })

        updateUIBarkMode(prefs)
    }

    private fun updateUIBarkMode(prefs: PrivateTracerPreferences) {
        settingsBarkModeSeekBar.isEnabled = prefs.isBarkModeEnabled()
        val sensitivity = prefs.getBarkModeSensitivity()
        settingsBarkModeSeekBar.progress = sensitivity
        val dBm = BarkModeEstimator.calcThresholdFromSensitivity(sensitivity)
        settingsBarkModeDesc2.text =
            getString(R.string.vibrate_on_detection_sensitivity).format(
                sensitivity.toString(),
                dBm.toString(),
                BarkModeEstimator.estimateDistanceFromDbm(dBm)
            )
    }

    private fun deleteAllData() {
        val activityManager = context?.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        activityManager.clearApplicationUserData()
        dismiss()
    }

    private fun navigateToRivmAdvicesWebPage() {
        val intent = Intent().apply {
            action = Intent.ACTION_VIEW
            addCategory(Intent.CATEGORY_BROWSABLE)
            data = Uri.parse(getString(R.string.rivm_advices_site_url))
        }
        startActivity(intent)
    }
}
