package org.privatetracer.view.iminfected

import android.os.Handler
import android.util.Base64
import android.util.Log
import android.view.View
import kotlinx.android.synthetic.main.sheet_iminfected.view.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.privatetracer.AuthCodeGenerator
import org.privatetracer.ConfigurableConstants.Companion.QUARANTINE_DAYS
import org.privatetracer.R
import org.privatetracer.api.TracerApi
import org.privatetracer.api.TracerDto
import org.privatetracer.api.TracerItemDto
import org.privatetracer.extensions.hide
import org.privatetracer.extensions.show
import org.privatetracer.storage.PrivateTracerDatabase
import org.privatetracer.view.BaseBottomSheet

class ImInfectedSheet : BaseBottomSheet() {
    companion object {
        private const val TAG = "ImInfectedSheet"
    }

    private val handler = Handler()

    override val layoutResourceId: Int
        get() = R.layout.sheet_iminfected

    override fun initUI(rootView: View) {
        rootView.iminfectedCloseBtn.setOnClickListener {
            dismiss()
        }

        CoroutineScope(Dispatchers.IO).launch {
            val authCode = sendTracers()
            handler.post {
                if (authCode != null) {
                    updateUI(rootView, authCode)
                } else {
                    updateUIError(rootView)
                }
            }
        }
    }

    private fun updateUI(rootView: View, authCode: String) {
        // AuthCode
        rootView.iminfectedNumberTv.show()
        rootView.iminfectedNumberTv.text = addDashes(authCode)

        // Other views
        rootView.iminfectedProgress.hide()
        rootView.iminfectedDesc1.show()
        rootView.iminfectedDesc2.show()
        rootView.iminfectedDesc2.text = String.format(getString(R.string.iminfected_desc2), QUARANTINE_DAYS)
    }

    private fun updateUIError(rootView: View) {
        rootView.iminfectedProgress.hide()
        rootView.iminfectedError.show()
    }

    private suspend fun sendTracers(): String? {
        return try {
            val authCode = AuthCodeGenerator.generateAuthCode()
            val items = PrivateTracerDatabase.instance.tracerKeyDao()
                .getTracerKeys()
                .map { TracerItemDto(Base64.encodeToString(it.seed, Base64.NO_WRAP), it.epoch) }
            if (items.isNotEmpty()) {
                Log.d(TAG, "Send $items seeds")
                val result = TracerApi.instance.service.uploadTracers(TracerDto(authCode, items))
                Log.d(TAG, "Seeds sent, got result: $result")
            } else {
                Log.d(TAG, "No seeds to send")
            }
            authCode
        } catch (t: Throwable) {
            // TODO (#63): improve error handling / user feedback
            Log.e(TAG, "Error sending seeds: ", t)
            null
        }
    }

    private fun addDashes(authCode: String): String {
        return authCode.substring(0..2) + "-" +
                authCode.substring(3..5) + "-" +
                authCode.substring(6..9)
    }
}

