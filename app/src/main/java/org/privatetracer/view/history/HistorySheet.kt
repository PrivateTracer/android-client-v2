package org.privatetracer.view.history

import android.view.View
import kotlinx.android.synthetic.main.sheet_history.view.*
import org.privatetracer.ConfigurableConstants.Companion.CONTACTS_PERSIST_DAYS
import org.privatetracer.ConfigurableConstants.Companion.EPH_ID_PERSIST_DAYS
import org.privatetracer.R
import org.privatetracer.view.BaseBottomSheet
import kotlin.math.max

class HistorySheet : BaseBottomSheet() {

    override val layoutResourceId: Int
        get() = R.layout.sheet_history

    override fun initUI(rootView: View) {
        rootView.historyCloseBtn.setOnClickListener {
            dismiss()
        }

        val maxDataPersistDays = max(CONTACTS_PERSIST_DAYS, EPH_ID_PERSIST_DAYS)
        rootView.informationSubText1.text = String.format(getString(R.string.history_info_desc1), maxDataPersistDays)
        rootView.informationSubText2.text = String.format(getString(R.string.history_info_desc2), maxDataPersistDays)
    }
}
