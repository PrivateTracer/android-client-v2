package org.privatetracer.view.error

import android.view.View
import kotlinx.android.synthetic.main.sheet_error.view.*
import org.privatetracer.R
import org.privatetracer.view.BaseBottomSheet

class ErrorSheet(private val errorText: String): BaseBottomSheet() {
    override val layoutResourceId: Int
        get() = R.layout.sheet_error

    override fun initUI(rootView: View) {
        rootView.errorCloseButton.setOnClickListener {
            dismiss()
        }

        rootView.error_text.text = errorText
    }
}