package org.privatetracer.view

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.FrameLayout
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetBehavior.BottomSheetCallback
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import org.privatetracer.R

abstract class BaseBottomSheet : BottomSheetDialogFragment() {
    private var lastKnownState: Int = 0
    private lateinit var bottomSheet: FrameLayout
    abstract val layoutResourceId: Int

    private val onShowListener = DialogInterface.OnShowListener {
        BottomSheetBehavior.from(bottomSheet).state = BottomSheetBehavior.STATE_EXPANDED
    }

    abstract fun initUI(rootView: View)

    override fun setupDialog(dialog: Dialog, style: Int) {
        val contentView = LayoutInflater.from(context).inflate(layoutResourceId, null)
        dialog.setContentView(contentView)

        bottomSheet = dialog.findViewById<View>(R.id.design_bottom_sheet) as FrameLayout
        bottomSheet.background = null
        bottomSheet.layoutParams.height = ViewGroup.LayoutParams.MATCH_PARENT

        val behavior = BottomSheetBehavior.from(bottomSheet)
        behavior.addBottomSheetCallback(object : BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                lastKnownState = newState
                if (newState == BottomSheetBehavior.STATE_COLLAPSED || newState == BottomSheetBehavior.STATE_HIDDEN) {
                    dismiss()
                }
            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {}
        })

        behavior.peekHeight = getDisplay(dialog.context).heightPixels
        behavior.isFitToContents = false
        behavior.state = BottomSheetBehavior.STATE_EXPANDED

        contentView.findViewById<View>(android.R.id.content)?.let { content ->
            content.layoutParams.height =
                getDisplay(dialog.context).heightPixels
        }

        dialog.setOnShowListener(onShowListener)

        initUI(contentView.rootView)
    }

    private fun getDisplay(context: Context): DisplayMetrics {
        val display = DisplayMetrics()
        val windowManager = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        windowManager.defaultDisplay.getMetrics(display)
        return display
    }
}
