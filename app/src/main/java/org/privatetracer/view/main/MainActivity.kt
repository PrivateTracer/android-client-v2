package org.privatetracer.view.main

import android.Manifest.permission.*
import android.bluetooth.BluetoothManager
import android.content.*
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.IBinder
import android.os.PowerManager
import android.provider.Settings
import android.text.format.DateUtils
import android.text.format.DateUtils.MINUTE_IN_MILLIS
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.privatetracer.BuildConfig
import org.privatetracer.ConfigurableConstants.Companion.QUARANTINE_DAYS
import org.privatetracer.R
import org.privatetracer.bluetooth.ForegroundService
import org.privatetracer.extensions.*
import org.privatetracer.services.RefreshStatusTextRunnable
import org.privatetracer.services.RefreshTracingSetManager
import org.privatetracer.services.RefreshTracingSetManager.Companion.REFRESH_TRACING_SETS
import org.privatetracer.storage.DataRetentionManager.DataRetentionValidator
import org.privatetracer.storage.NOT_REFRESHED_BEFORE
import org.privatetracer.storage.PrivateTracerPreferences
import org.privatetracer.view.error.ErrorSheet
import org.privatetracer.view.history.HistorySheet
import org.privatetracer.view.iminfected.ImInfectedSheet
import org.privatetracer.view.info.InfoSheet
import org.privatetracer.view.main.dialog.TurnTracingOffConfirmationDialog
import org.privatetracer.view.settings.SettingsSheet
import java.util.*

private const val TAG = "MainActivity"
private const val REQUEST_PERMISSIONS = 1
private val REQUIRED_PERMISSIONS = arrayListOf(ACCESS_FINE_LOCATION, BLUETOOTH).apply {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
        add(ACCESS_BACKGROUND_LOCATION)
    }
}.toTypedArray()

class MainActivity : AppCompatActivity(), ServiceConnection,
    TracingStatusServicesRunner {
    private lateinit var prefs: PrivateTracerPreferences
    private lateinit var tracingIntent: Intent

    private var isTracingTurnedOn = false
        set(value) {
            field = value
            updateUI()
        }

    private val refreshStatusTextRunnable = RefreshStatusTextRunnable()
    private val tracingStatusReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            when (intent?.action) {
                REFRESH_TRACING_SETS -> {
                    mainViewModel.refreshTracingSetsFromServer(prefs)
                }
                else -> Unit
            }
        }
    }

    private val mainViewModel by lazy { getViewModel { MainViewModel() } }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        prefs = PrivateTracerPreferences(applicationContext)
        tracingIntent = Intent(this, ForegroundService::class.java)

        DataRetentionValidator.start()
        startTracingStatusOnBackground()

        mainViewModel.daysAtRisk.observe(this, Observer { daysAtRisk ->
            updateUI(daysAtRisk)
            StatusChangeNotificationManager(this).showNotification(
                mainViewModel.getPreviousDaysAtRisk(),
                daysAtRisk
            )
        })

        mainRefreshBtn.show()

        mainCircle.setOnClickListener {
            toggleTracingService()
        }
        if (BuildConfig.DEBUG) {
            //Just for testing.
            mainLogoRO.setOnClickListener {
                mainViewModel.setDaysAtRisk(if (mainViewModel.getCurrentDaysAtRisk() > 0) 0 else QUARANTINE_DAYS)
            }
        }

        setRefreshStatusText()

        mainRefreshBtn.setOnClickListener {
            it.animate(R.anim.rotate_360_reverse)
            mainViewModel.refreshTracingSetsFromServer(prefs)
        }

        mainBtnClock.setOnClickListener {
            val sheet = HistorySheet()
            sheet.show(supportFragmentManager, "history_sheet")
        }

        mainWhatIsShared.setOnClickListener {
            val sheet = InfoSheet()
            sheet.show(supportFragmentManager, "info_sheet")
        }

        mainBtnImInfected.setOnClickListener {
            val sheet = ImInfectedSheet()
            sheet.show(supportFragmentManager, "iminfected_sheet")
            //Sending tracers has been moved to the iminfected sheet
        }

        mainBtnSettings.setOnClickListener {
            val sheet = SettingsSheet()
            sheet.show(supportFragmentManager, "settings_sheet")
        }
        mainViewModel.isDateInvalid.observe(this, Observer {
            if (it) {
                ErrorSheet(getString(R.string.invalide_date_error_text)).show(supportFragmentManager, "error_sheet")
            }
        })
    }

    override fun onResume() {
        super.onResume()
        bindService(tracingIntent, this, BIND_AUTO_CREATE)
        startRefreshStatusText()
    }

    override fun onPause() {
        stopRefreshStatusTextService()
        unbindService(this)
        super.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(tracingStatusReceiver)
    }

    override fun onServiceConnected(name: ComponentName?, iBinder: IBinder) {
        val foregroundService = (iBinder as ForegroundService.ServiceBinder).getService()
        isTracingTurnedOn = foregroundService.isScanning()
    }

    override fun onServiceDisconnected(name: ComponentName?) {
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            REQUEST_PERMISSIONS -> {
                if (grantResults.isEmpty() || grantResults.any { it != PackageManager.PERMISSION_GRANTED }) {
                    Log.i(TAG, "A permission is denied by the user.")
                } else {
                    toggleTracingService()
                }
            }
        }
    }

    private fun setRefreshStatusText() {
        val statusLastUpdatedMillis = prefs.getStatusLastRefreshedMillis()
        mainRefreshStatusTV.text =
            if (statusLastUpdatedMillis != NOT_REFRESHED_BEFORE) {
                val timeAgoText = DateUtils.getRelativeTimeSpanString(
                    statusLastUpdatedMillis, System.currentTimeMillis(), MINUTE_IN_MILLIS
                ).toString().formatCustomRelativeTimeSpanString()
                String.format(Locale.getDefault(), getString(R.string.last_time_updated), timeAgoText)
            } else {
                getString(R.string.start_tracing_to_refresh)
            }
    }

    private fun String.formatCustomRelativeTimeSpanString() =
        if (equals(getString(R.string.zero_minutes_ago))) {
            getString(R.string.zero_minutes_ago_converted)
        } else {
            replace(getString(R.string.minutes), getString(R.string.minutes_converted))
                .replace(getString(R.string.minute), getString(R.string.minute_converted))
        }

    private fun updateUI() {
        updateUI(mainViewModel.getCurrentDaysAtRisk())
    }

    private fun updateUI(daysAtRisk: Int) {
        if (daysAtRisk > 0) {
            // Visibility
            mainTurnOnLabel.hide()
            mainTurnOnDescription.hide()
            mainPhoneImage.hide()
            mainCircleInfectedProgress.show()
            mainCircleInfectedTitle.show()
            mainCircleInfectedCountdownTV.show()
            // Set values
            mainCircleInfectedProgress.max = QUARANTINE_DAYS
            mainCircleInfectedProgress.progress = daysAtRisk
            mainCircleInfectedCountdownTV.text =
                getString(R.string.infected_stay_x_days_at_home).format(daysAtRisk)
            mainInteractionsDetectedStatus.setTextColor(
                ContextCompat.getColor(
                    this,
                    R.color.colorMainDetected
                )
            )
            mainInteractionsDetectedStatus.text = getString(R.string.interaction_detected)
            // Background
            mainRoot.setBackgroundResource(R.drawable.main_background_at_risk)
        } else {
            // Visibility
            mainTurnOnLabel.show()
            mainTurnOnDescription.show()
            mainPhoneImage.show()
            mainCircleInfectedProgress.hide()
            mainCircleInfectedTitle.hide()
            mainCircleInfectedCountdownTV.hide()
            // Set values
            mainTurnOnLabel.text =
                getString(if (isTracingTurnedOn) R.string.tracing_turned_on else R.string.tracing_turned_off)
            mainTurnOnDescription.text =
                getString(if (isTracingTurnedOn) R.string.tracing_desc_turned_on else R.string.tracing_desc_turned_off)
            mainPhoneImage.setImageResource(if (isTracingTurnedOn) R.drawable.ic_phone_on else R.drawable.ic_phone_off)
            mainPhoneImage.contentDescription =
                getString(if (isTracingTurnedOn) R.string.description_phone_image_turned_on else R.string.description_phone_image_turned_off)
            mainInteractionsDetectedStatus.setTextColor(
                ContextCompat.getColor(
                    this,
                    R.color.colorMainNotDetected
                )
            )
            mainInteractionsDetectedStatus.text = getString(R.string.interaction_not_detected)
            // Background
            mainRoot.setBackgroundResource(if (isTracingTurnedOn) R.drawable.main_background_tracing_turned_on else R.drawable.main_background_tracing_turned_off)
        }

        // Animations
        mainCloud1.animate(R.anim.cloud1)
        mainCloud2.animate(R.anim.cloud2)
        mainCloud3.animate(R.anim.cloud3)
        mainCloud4.animate(R.anim.cloud4)

        setRefreshStatusText()
        handleTracingPing()

        //mainRefreshBtn.showIf(isTurnedOn)
    }

    private fun handleTracingPing() {
        tracingPingCircle1.showIf(isTracingTurnedOn)
        tracingPingCircle2.showIf(isTracingTurnedOn)
        if (!isTracingTurnedOn) {
            return
        }
        tracingPingCircle1.animate(R.anim.tracing_ping)
        tracingPingCircle2.animate(R.anim.tracing_ping, 200) {
            if (!isTracingTurnedOn) {
                return@animate
            }
            tracingPingCircle2.animate(R.anim.tracing_ping, 900) {
                if (!isTracingTurnedOn) {
                    return@animate
                }
                tracingPingCircle1.animate(R.anim.tracing_ping, 100)
                tracingPingCircle2.animate(R.anim.tracing_ping, 900) {
                    GlobalScope.launch(Dispatchers.IO) {
                        Thread.sleep(2000)
                        GlobalScope.launch(Dispatchers.Main) {
                            handleTracingPing()
                        }
                    }
                }
            }
        }
    }

    private fun toggleTracingService() {
        if (!isTracingTurnedOn) {
            if (isAnyPermissionNotGranted()) {
                ActivityCompat.requestPermissions(
                    this,
                    REQUIRED_PERMISSIONS,
                    REQUEST_PERMISSIONS
                )
            } else {
                isTracingTurnedOn = true
                startTracingService()
            }
        } else {
            TurnTracingOffConfirmationDialog(this) {
                isTracingTurnedOn = false
                stopService(tracingIntent)
            }.show()
        }
    }

    private fun startTracingService() {
        enableBluetoothIfNeeded()
        requestBatteryIfNeeded()
        ContextCompat.startForegroundService(this, tracingIntent)
    }

    private fun requestBatteryIfNeeded() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!prefs.isBatteryOptimizationPermissionRequested()) {
                val powerManager = getSystemService(Context.POWER_SERVICE) as PowerManager

                if (!powerManager.isIgnoringBatteryOptimizations(packageName)) {
                    try {
                        val intent = Intent()
                        intent.action = Settings.ACTION_IGNORE_BATTERY_OPTIMIZATION_SETTINGS
                        startActivity(intent)
                    } catch (e: Exception) {
                        //ignore
                    }
                }
            }
        }
        //Don't ask again.
        prefs.setBatteryOptimizationPermissionRequested(true)
    }

    private fun isAnyPermissionNotGranted(): Boolean {
        return REQUIRED_PERMISSIONS.any {
            ContextCompat.checkSelfPermission(
                this,
                it
            ) != PackageManager.PERMISSION_GRANTED
        }
    }

    private fun enableBluetoothIfNeeded() {
        val bluetoothManager = getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        if (!bluetoothManager.adapter.isEnabled) {
            bluetoothManager.adapter.enable()
        }
    }

    override fun startTracingStatusOnBackground() {
        startRefreshTracingSetsOnBackground()
        startRefreshStatusText()
    }

    override fun stopTracingStatusOnBackground() {
        RefreshTracingSetManager.stop()
        stopRefreshStatusTextService()
        unregisterReceiver(tracingStatusReceiver)
    }

    private fun startRefreshTracingSetsOnBackground() {
        if (prefs.isAutoMatchEnabled()) {
            RefreshTracingSetManager.start()
            registerReceiver(tracingStatusReceiver, IntentFilter().apply { addAction(REFRESH_TRACING_SETS) })
        }
    }

    private fun startRefreshStatusText() {
        if (prefs.isAutoMatchEnabled()) {
            refreshStatusTextRunnable.start {
                setRefreshStatusText()
            }
        }
    }

    private fun stopRefreshStatusTextService() {
        refreshStatusTextRunnable.stop()
    }
}

interface TracingStatusServicesRunner {
    fun startTracingStatusOnBackground()
    fun stopTracingStatusOnBackground()
}
