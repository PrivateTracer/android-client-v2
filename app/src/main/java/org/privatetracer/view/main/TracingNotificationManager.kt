package org.privatetracer.view.main

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager.IMPORTANCE_LOW
import android.content.Context
import androidx.core.app.NotificationCompat
import org.privatetracer.R
import org.privatetracer.view.CHANNEL_ID_TRACING
import org.privatetracer.view.PrivateTracerNotificationManager

class TracingNotificationManager(context: Context) : PrivateTracerNotificationManager(context) {

    fun createNotification(): Notification {
        createNotificationChannel()

        return NotificationCompat.Builder(this, CHANNEL_ID_TRACING)
            .setContentTitle(getString(R.string.notification_tracing_title))
            .setSmallIcon(R.mipmap.ic_launcher)
            .setContentIntent(getPendingIntentResumeApp())
            .build()
    }

    private fun createNotificationChannel() {
        if (shouldCreateNotificationChannel()) {
            val channelName = getString(R.string.notification_channel_tracing)
            val serviceChannel = NotificationChannel(CHANNEL_ID_TRACING, channelName, IMPORTANCE_LOW)
            createNotificationChannel(serviceChannel)
        }
    }
}
