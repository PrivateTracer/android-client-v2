package org.privatetracer.view.main

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.privatetracer.api.TracerApi
import org.privatetracer.api.models.Configuration
import org.privatetracer.contact.ContactManager
import org.privatetracer.contact.TracingSetIdsDao
import org.privatetracer.storage.PrivateTracerDatabase
import org.privatetracer.storage.PrivateTracerPreferences
import org.privatetracer.storage.model.TracingSetId

class MainViewModel(
    // TODO: properly implement dependency injection, so that we can declare ONCE an instance
    private val tracingSetIdsDao: TracingSetIdsDao = PrivateTracerDatabase.instance.tracingSetIdsDao()
) : ViewModel() {

    companion object {
        private const val TAG = "MainViewModel"
    }

    private val service = TracerApi.instance.service

    // TODO: move to a repository class and saved in prefs
    private var _previousDaysAtRisk: Int? = null
    private val _daysAtRisk = MutableLiveData(0)
    val isDateInvalid = MutableLiveData(false)

    val daysAtRisk: LiveData<Int>
        get() = _daysAtRisk

    private suspend fun getConfig(): Configuration {
        return service.getConfiguration()
    }

    fun getNewestTracingSetIds(
        newTracingSetIds: List<String>,
        localTracingSetIds: List<TracingSetId>
    ): List<TracingSetId> {
        val localTracingSetIdsStrings = localTracingSetIds.map { it.id }

        // Excluding from newTracingSetIds elements that are not in localTracingSetIds (newest ids)
        return newTracingSetIds.filter { !localTracingSetIdsStrings.contains(it) }.map { TracingSetId(it) }
    }

    /**
     * Delete the TracingSetId's that are irrelevant to maintain: I.e. the `localTracingSetIds` from the database
     * that aren't included in the `newTracingSetIds` from the API are considered too old and can be removed
     */
    suspend fun deleteOldestTracingSetIds(
        newTracingSetIds: List<String>,
        localTracingSetIds: List<TracingSetId>
    ) {
        localTracingSetIds.forEach {
            if (!newTracingSetIds.contains(it.id)) {
                tracingSetIdsDao.deleteTracingSetId(it)
            }
        }
    }

    fun getCurrentDaysAtRisk(): Int {
        return _daysAtRisk.value ?: 0
    }

    fun getPreviousDaysAtRisk(): Int {
        return _previousDaysAtRisk ?: getCurrentDaysAtRisk()
    }

    fun setDaysAtRisk(newValue: Int) {
        _previousDaysAtRisk = getCurrentDaysAtRisk()
        _daysAtRisk.postValue(newValue)
    }

    fun refreshTracingSetsFromServer(prefs: PrivateTracerPreferences) {
        CoroutineScope(Dispatchers.IO).launch {
            var newTracingSetIds: List<String> = emptyList()
            try {
                newTracingSetIds = getConfig().tracingSet.ids
            } catch (e: Exception) {
                isDateInvalid.postValue(true)
                // TODO (#63): improve user feedback (not always isDateInvalid!)
                Log.w(TAG, "Error on refreshing data from server: ", e)
            }

            if (isDateInvalid.value == false) {
                val localTracingSetIds = tracingSetIdsDao.getAll()
                deleteOldestTracingSetIds(newTracingSetIds, localTracingSetIds)

                val newestTracingSetIds = getNewestTracingSetIds(newTracingSetIds, localTracingSetIds)

                newestTracingSetIds.forEach { tracingSetId ->
                    val cuckooFilterJson = getCuckooFilterJson(tracingSetId.id)
                    cuckooFilterJson?.let { json ->
                        // If anything went wrong when retrieving the cuckooFilter, then the cuckooFilterJson is null:
                        // In that case we shouldn't try to updateInfectedContacts
                        ContactManager.instance.updateInfectedContacts(json)

                        // If everything went as expected, then we've updated the infectedContacts successfully:
                        // Now we can save the tracingSetId locally, so we won't retrieve the data again the next time
                        tracingSetIdsDao.addTracingSetId(tracingSetId)
                    }
                }
                prefs.updateStatusLastRefreshed()
                val daysLeft = ContactManager.instance.getDaysAtRisk()
                Log.d(TAG, "Checked infected: $daysLeft")

                setDaysAtRisk(daysLeft)
            }
        }
    }

    private suspend fun getCuckooFilterJson(id: String): String? {
        try {
            val tracingSet = service.getTracingSet(id)
            return tracingSet.content
        } catch (t: Throwable) {
            // TODO #63: Implement an onError flow
            Log.e(TAG, "Error loading TracingSets: ", t)
        }
        return null
    }
}
