package org.privatetracer.view.main.dialog

import android.app.Activity
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Window
import kotlinx.android.synthetic.main.dialog_turn_off_tracing_confimation.*
import org.privatetracer.R
import org.privatetracer.extensions.hide

class TurnTracingOffConfirmationDialog(
    activity: Activity, private val onTurnTracingOff: () -> Unit
) : Dialog(activity) {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.dialog_turn_off_tracing_confimation)

        popup_title.text = getString(R.string.popup_title_turn_off_tracing_confirmation)
        popup_message.text = getString(R.string.popup_message_turn_off_tracing_confirmation)

        button_keep_on.setOnClickListener { dismiss() }
        button_turn_off.setOnClickListener {
            onTurnTracingOff.invoke()
            dismiss()
        }
        // TODO: this button doesn't do anything right now (design hasn't been finished yet), so hide it for now
        button_battery_consumption.hide()
        button_battery_consumption.setOnClickListener { dismiss() }

        setInvisibleBackground() // To prevent white corners
    }

    private fun setInvisibleBackground() {
        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    }

    private fun getString(title: Int) = context.getString(title)
}
