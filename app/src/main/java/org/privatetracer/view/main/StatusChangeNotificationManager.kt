package org.privatetracer.view.main

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import androidx.core.app.NotificationCompat
import org.privatetracer.R
import org.privatetracer.view.main.StatusChangeNotificationManager.StatusChange.CHANGED_TO_AT_RISK
import org.privatetracer.view.main.StatusChangeNotificationManager.StatusChange.NOT_AT_RISK_ANYMORE
import org.privatetracer.view.CHANNEL_ID_STATUS_AT_RISK
import org.privatetracer.view.CHANNEL_ID_STATUS_SAFE
import org.privatetracer.view.NOTIFICATION_ID_INFECTION_STATUS
import org.privatetracer.view.PrivateTracerNotificationManager

class StatusChangeNotificationManager(context: Context) : PrivateTracerNotificationManager(context) {

    enum class StatusChange {
        CHANGED_TO_AT_RISK,
        NOT_AT_RISK_ANYMORE
    }

    fun showNotification(previousAtRiskValue: Int, newAtRiskValue: Int) {
        when {
            newAtRiskValue > 0 && previousAtRiskValue <= 0 -> createStatusNotification(CHANGED_TO_AT_RISK)
            previousAtRiskValue > 0 && newAtRiskValue <= 0 -> createStatusNotification(NOT_AT_RISK_ANYMORE)
            else -> null
        }?.let {
            showNotification(NOTIFICATION_ID_INFECTION_STATUS, it)
        }
    }

    private fun createStatusNotification(statusChange: StatusChange): Notification {
        createStatusNotificationChannel(statusChange)

        val notificationTitle = when (statusChange) {
            CHANGED_TO_AT_RISK -> getString(R.string.notification_status_change_title_at_risk)
            NOT_AT_RISK_ANYMORE -> getString(R.string.notification_status_change_title_safe)
        }
        val notificationMessage = when (statusChange) {
            CHANGED_TO_AT_RISK -> getString(R.string.notification_status_change_message_at_risk)
            NOT_AT_RISK_ANYMORE -> getString(R.string.notification_status_change_message_safe)
        }
        val notificationPriority = when (statusChange) {
            CHANGED_TO_AT_RISK -> NotificationCompat.PRIORITY_MAX
            NOT_AT_RISK_ANYMORE -> NotificationCompat.PRIORITY_DEFAULT
        }

        return NotificationCompat.Builder(this, CHANNEL_ID_STATUS_AT_RISK)
            .setContentTitle(notificationTitle)
            .setContentText(notificationMessage)
            .setSmallIcon(R.drawable.ic_alert_triangle)
            .setPriority(notificationPriority)
            .setContentIntent(getPendingIntentResumeApp())
            .setAutoCancel(true)
            .build()
    }

    private fun createStatusNotificationChannel(statusChange: StatusChange) {
        if (shouldCreateNotificationChannel()) {
            val channel = when (statusChange) {
                CHANGED_TO_AT_RISK -> NotificationChannel(
                    CHANNEL_ID_STATUS_AT_RISK,
                    getString(R.string.notification_channel_status_change_at_risk),
                    NotificationManager.IMPORTANCE_HIGH
                )
                NOT_AT_RISK_ANYMORE -> NotificationChannel(
                    CHANNEL_ID_STATUS_SAFE,
                    getString(R.string.notification_channel_status_change_safe),
                    NotificationManager.IMPORTANCE_DEFAULT
                )
            }
            createNotificationChannel(channel)
        }
    }
}
