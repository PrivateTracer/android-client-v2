package org.privatetracer.bluetooth

import android.bluetooth.*
import android.content.Context
import android.os.Build
import android.util.Log
import java.util.*

class GattSpinner(val context: Context) {
    fun createJobs(bluetoothDevice: BluetoothDevice, resultListener: (UUID) -> Unit): GattApple {
        return GattApple(context, bluetoothDevice, resultListener)
    }
}

class GattApple(
    val context: Context,
    var bluetoothDevice: BluetoothDevice,
    val resultListener: (UUID) -> Unit
) {
    private var bluetoothGatt: BluetoothGatt? = null
    private var startTime: Long = 0
    fun execute() {
        Log.d(TAG, "connected")
        val gattCallback: BluetoothGattCallback = object : BluetoothGattCallback() {
            override fun onConnectionStateChange(
                gatt: BluetoothGatt,
                status: Int,
                state: Int
            ) {
                super.onConnectionStateChange(gatt, status, state)
                when (state) {
                    BluetoothProfile.STATE_CONNECTING -> {
                        Log.d(TAG, "connecting... $status")
                    }
                    BluetoothProfile.STATE_CONNECTED -> {
                        Log.d(TAG, "connected $status")
                        gatt.requestMtu(512) // TODO: tweak
                    }
                    BluetoothProfile.STATE_DISCONNECTED -> {
                        Log.d(TAG, "disconnected $status")
                        stop()
                    }
                };
            }

            override fun onMtuChanged(
                gatt: BluetoothGatt,
                mtu: Int,
                status: Int
            ) {
                Log.d(TAG, "starting discovery")
                gatt.discoverServices()
            }

            override fun onServicesDiscovered(
                gatt: BluetoothGatt,
                status: Int
            ) {
                val service = gatt.getService(UUID.fromString(ID_UUID))
                if (service == null) {
                    Log.e(
                        TAG,
                        "GATT service for ${ID_UUID} not found, status=${status}"
                    )
                } else {
                    Log.d(
                        TAG,
                        "Service ${service.uuid} found"
                    )
                    val characteristic = service.characteristics.first()
                    Log.d(TAG, "We received ${characteristic.uuid} from iOS")
                    resultListener(characteristic.uuid)
                }
                stop()
            }

            override fun onCharacteristicRead(
                gatt: BluetoothGatt,
                characteristic: BluetoothGattCharacteristic,
                status: Int
            ) {
                // TODO this should never happen
                stop()
            }
        }
        bluetoothGatt = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            bluetoothDevice.connectGatt(
                context,
                false,
                gattCallback,
                BluetoothDevice.TRANSPORT_LE
            )
        } else {
            bluetoothDevice.connectGatt(context, false, gattCallback)
        }
        startTime = System.currentTimeMillis()
    }

    fun stop() {
        bluetoothGatt?.close()
        bluetoothGatt = null
    }

    companion object {
        private const val TAG = "GATT"
    }
}


