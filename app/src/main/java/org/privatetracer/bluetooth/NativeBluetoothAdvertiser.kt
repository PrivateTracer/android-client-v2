package org.privatetracer.bluetooth

import android.bluetooth.le.AdvertiseCallback
import android.bluetooth.le.AdvertiseData
import android.bluetooth.le.AdvertiseSettings
import android.content.Context
import android.util.Log

class NativeBluetoothAdvertiser(context: Context) : NativeBluetoothService(context),
    IBluetoothAdvertiser {
    private var advertiseCallback: AdvertiseCallback? = null

    private val isBroadcasting: Boolean
        get() = advertiseCallback != null

    override fun broadcast(payload: ByteArray) {
        stopBroadcasting()
        val settings = AdvertiseSettings.Builder()
            .setTimeout(0) // don't listen to Apple
            .setConnectable(false)
            .build()

        val advertiseData = AdvertiseData.Builder()
            .addServiceData(ID_UUID.toParcelUUID(), payload)
            .build()

        advertiseCallback = object : AdvertiseCallback() {
            override fun onStartFailure(errorCode: Int) {
                Log.e("BROADCAST", errorCode.toString())
            }

            override fun onStartSuccess(settingsInEffect: AdvertiseSettings?) {
                Log.i("BROADCAST", "Advertisement started")
            }
        }
        adapter.bluetoothLeAdvertiser.startAdvertising(settings, advertiseData, advertiseCallback)
    }

    override fun stopBroadcasting() {
        if (!isBroadcasting) {
            return
        }

        adapter.bluetoothLeAdvertiser.stopAdvertising(advertiseCallback)
    }
}
