package org.privatetracer.bluetooth

import android.bluetooth.le.ScanCallback
import android.bluetooth.le.ScanFilter
import android.bluetooth.le.ScanResult
import android.bluetooth.le.ScanSettings
import android.content.Context
import android.util.Log

class NativeBluetoothScanner(context: Context) : NativeBluetoothService(context),
    IBluetoothScanner<ScanResult> {
    private var scanCallback: ScanCallback? = null

    override val isScanning: Boolean
        get() = scanCallback != null

    override fun scan(listener: ScanListener<ScanResult>?) {
        if (isScanning) {
            return
        }
        val filters = listOf(
            ScanFilter.Builder() // android private tracer filter
                .setServiceData(ID_UUID.toParcelUUID(), ByteArray(16), ByteArray(16))
                .build(),
            ScanFilter.Builder() //  possible ios private tracer filter
                .setServiceUuid(ID_UUID.toParcelUUID(), MASK.toParcelUUID())
                .build()
        )

        val settings = ScanSettings.Builder()
            .setReportDelay(300)
            .setScanMode(ScanSettings.SCAN_MODE_BALANCED)
            .build()

        scanCallback = object : ScanCallback() {
            override fun onBatchScanResults(results: MutableList<ScanResult>?) {
                if (listener == null) {
                    return
                }

                results?.forEach { listener.onScanResult(it) }
            }

            override fun onScanResult(callbackType: Int, result: ScanResult?) {
                if (result != null) listener?.onScanResult(result)
            }

            override fun onScanFailed(errorCode: Int) {
                Log.e("SCAN", errorCode.toString())
            }
        }
        adapter.bluetoothLeScanner.startScan(filters, settings, scanCallback)
    }

    override fun scan(onScanResult: (ScanResult) -> Unit) {
        scan(object : ScanListener<ScanResult> {
            override fun onScanResult(result: ScanResult) {
                onScanResult(result)
            }
        })
    }

    override fun stopScanning() {
        if (!isScanning) {
            return
        }

        adapter.bluetoothLeScanner.stopScan(scanCallback)
        scanCallback = null
    }
}
