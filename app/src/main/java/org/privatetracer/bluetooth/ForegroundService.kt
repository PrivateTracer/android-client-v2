package org.privatetracer.bluetooth

import android.app.Service
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Binder
import android.os.IBinder
import android.util.Log
import org.privatetracer.view.NOTIFICATION_ID_TRACING
import org.privatetracer.view.main.TracingNotificationManager
import org.privatetracer.contact.ContactManager
import org.privatetracer.contact.ContactTracingService
import org.privatetracer.parser.ScanResultParser
import org.privatetracer.storage.PrivateTracerDatabase

class ForegroundService : Service() {
    private val binder = ServiceBinder()
    private lateinit var tracingService: ContactTracingService

    inner class ServiceBinder : Binder() {
        fun getService(): ForegroundService {
            return this@ForegroundService
        }
    }

    override fun onBind(intent: Intent?): IBinder? {
        return binder
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        val notification = TracingNotificationManager(this).createNotification()
        startForeground(NOTIFICATION_ID_TRACING, notification)

        if (!checkIsBluetoothEnabled(this)) {
            //wait for bluetooth to be on.
            val filter = IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED)
            this.registerReceiver(bluetoothStateReceiver, filter)
            enableBluetooth(this)
        } else {
            startTracingService()
        }
        return START_STICKY
    }

    private fun startTracingService() {
        val db = PrivateTracerDatabase.instance
        tracingService =
            ContactTracingService(
                NativeBluetoothAdvertiser(this),
                NativeBluetoothScanner(this),
                // TODO: use local storage instead of in memory
                // TODO: use a better estimator of actual contact instead of the FakeContactEstimator
                ContactManager.instance,
                db.tracerKeyDao(),
                GattSpinner(this),
                ScanResultParser()
            )
        tracingService.start()
    }

    override fun onDestroy() {
        Log.d("SERVICE", "Tearing down foreground service")
        if (this::tracingService.isInitialized) {
            tracingService.close()
        }

        try {
            unregisterReceiver(bluetoothStateReceiver)
        } catch (e: IllegalArgumentException) {
            //ignore
        }
        super.onDestroy()
    }

    private val bluetoothStateReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val action = intent.action
            if (action == BluetoothAdapter.ACTION_STATE_CHANGED) {
                val state = intent.getIntExtra(
                    BluetoothAdapter.EXTRA_STATE,
                    BluetoothAdapter.ERROR
                )
                when (state) {
                    BluetoothAdapter.STATE_ON -> {
                        Log.i("Bluetooth", "Bluetooth on")
                        //retry starting tracing
                        startTracingService()
                    }
                    BluetoothAdapter.STATE_OFF -> Log.i("Bluetooth", "Bluetooth off")
                    BluetoothAdapter.STATE_TURNING_OFF -> Log.i(
                        "Bluetooth",
                        "Turning Bluetooth off..."
                    )
                    BluetoothAdapter.STATE_TURNING_ON -> Log.i(
                        "Bluetooth",
                        "Turning Bluetooth on..."
                    )
                }
            }
        }
    }

    private fun checkIsBluetoothEnabled(context: Context): Boolean {
        val bluetoothManager =
            context.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        return bluetoothManager.adapter.isEnabled
    }

    private fun enableBluetooth(context: Context): Boolean {
        val bluetoothManager =
            context.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        return bluetoothManager.adapter.enable()
    }

    fun isScanning() = this::tracingService.isInitialized && tracingService.isScanning()
}
