package org.privatetracer.bluetooth

interface IBluetoothScanner<T> {
    /**
     * Scan the peripheral for BLE advertisement. Obtained results will be forwarded to the listener.
     * Scanning is optimized for the service UUID's of org.privatetracer.bluetooth.Constants
     */
    fun scan(listener: ScanListener<T>?)

    fun scan(onScanResult: (T) -> Unit)

    /**
     * Stops scanning for advertisement (e.g. when the user wants to stop tracking)
     */
    fun stopScanning()

    val isScanning: Boolean
}

interface IBluetoothAdvertiser {
    /**
     * Advertise a payload using BLE. Can be called again if the advertised payload needs to change.
     * (This will stop previous broadcasts)
     */
    fun broadcast(payload: ByteArray)

    /**
     * Stop advertising.
     */
    fun stopBroadcasting()
}

interface ScanListener<T> {
    fun onScanResult(result: T)
}
