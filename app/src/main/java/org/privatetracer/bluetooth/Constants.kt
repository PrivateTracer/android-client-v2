package org.privatetracer.bluetooth

import android.os.ParcelUuid

// The 16-bit UUID that is associated with the key data of identifying an interaction
const val ID_UUID = "00001D1D-0000-1000-8000-00805F9B34FB"

// Apple's manufacturer ID for iBeacons
const val APPLE_ID = 0x004c
const val MASK = "0000FFFF-0000-0000-0000-000000000000"

fun String.toParcelUUID(): ParcelUuid = ParcelUuid.fromString(this)