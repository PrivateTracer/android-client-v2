package org.privatetracer

import java.security.SecureRandom

class AuthCodeGenerator {
    companion object {
        private const val VALID_CHARS = "ABCDEFGHJKLMNPRSTUVWXYZ23456789"

        /**
         * Generate random code, should be 10 chars long and contain checksum.
         * Does *not* include dashes, add this when needed (in the UI only)
         * See: [https://gitlab.com/PrivateTracer/caregiversportal/-/blob/develop/AuthenticationCodes.md]
         */
        fun generateAuthCode(): String {
            val sb = StringBuffer()
            val secureRandom = SecureRandom()
            //9 random chars
            for (i in 1..9) {
                val r = secureRandom.nextInt(VALID_CHARS.length)
                sb.append(characterFromCodePoint(r))
            }
            //Add checksum
            sb.append(generateCheckCharacter(sb.toString()))
            return sb.toString()
        }

        /**
         *  Generate checksum char
         * see: [https://en.wikipedia.org/wiki/Luhn_mod_N_algorithm]
         */
        private fun generateCheckCharacter(input: String): Char {
            var factor = 2
            var sum = 0
            val n: Int = VALID_CHARS.length
            // Starting from the right and working leftwards is easier since
            // the initial "factor" will always be "2".
            for (i in input.length - 1 downTo 0) {
                val codePoint: Int = codePointFromCharacter(input[i])
                var addend = factor * codePoint
                // Alternate the "factor" that each "codePoint" is multiplied by
                factor = if (factor == 2) 1 else 2
                // Sum the digits of the "addend" as expressed in base "n"
                addend = addend / n + addend % n
                sum += addend
            }
            // Calculate the number that must be added to the "sum"
            // to make it divisible by "n".
            val remainder = sum % n
            val checkCodePoint = (n - remainder) % n
            return characterFromCodePoint(checkCodePoint)
        }

        private fun characterFromCodePoint(codePoint: Int): Char {
            return VALID_CHARS[codePoint]
        }

        // ===== Test / validation methods ===== //

        /**
         * Validates correctness of AuthCode via the Checksum
         */
        fun validateAuthCode(authCode: String): Boolean {
            return validateCheckCharacter(removeDashes(authCode))
        }

        private fun removeDashes(authCode: String): String {
            return authCode.replace("-", "")
        }

        private fun validateCheckCharacter(input: String): Boolean {
            var factor = 1
            var sum = 0
            val n: Int = VALID_CHARS.length
            // Starting from the right, work leftwards
            // Now, the initial "factor" will always be "1"
            // since the last character is the check character.
            for (i in input.length - 1 downTo 0) {
                val codePoint = codePointFromCharacter(input[i])
                var addend = factor * codePoint
                // Alternate the "factor" that each "codePoint" is multiplied by
                factor = if (factor == 2) 1 else 2
                // Sum the digits of the "addend" as expressed in base "n"
                addend = addend / n + addend % n
                sum += addend
            }
            val remainder = sum % n
            return remainder == 0
        }

        private fun codePointFromCharacter(char: Char): Int {
            return VALID_CHARS.indexOf(char)
        }
    }
}
