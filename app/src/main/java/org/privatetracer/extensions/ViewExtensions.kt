package org.privatetracer.extensions

import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.annotation.AnimRes

fun View.show() {
    visibility = VISIBLE
}

fun View.hide() {
    visibility = GONE
}

fun View.showIf(condition: Boolean) {
    if (condition) show() else hide()
}

fun View.animate(
    @AnimRes animRes: Int, offsetMillis: Long = 0, onAnimationEnd: () -> Unit = {}
) {
    val animation = AnimationUtils.loadAnimation(context, animRes)
    animation.startOffset = offsetMillis
    animation.setAnimationListener(object : Animation.AnimationListener {
        override fun onAnimationStart(animation: Animation) {}
        override fun onAnimationRepeat(animation: Animation) {}
        override fun onAnimationEnd(animation: Animation) {
            onAnimationEnd()
        }
    })
    startAnimation(animation)
}
