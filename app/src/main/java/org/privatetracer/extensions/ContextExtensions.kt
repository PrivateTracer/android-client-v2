package org.privatetracer.extensions

import android.content.Context

inline fun <reified T : Any> Context.toInterface(): T {
    require(this is T) { "$this does not implement this interface" }
    return this
}
