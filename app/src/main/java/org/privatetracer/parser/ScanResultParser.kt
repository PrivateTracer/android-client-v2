package org.privatetracer.parser

import android.bluetooth.le.ScanResult
import android.os.ParcelUuid
import android.os.SystemClock
import android.util.Log
import org.privatetracer.bluetooth.APPLE_ID
import org.privatetracer.bluetooth.ID_UUID
import org.privatetracer.dp3t.Dp3t
import org.privatetracer.dp3t.EpochInfo
import org.privatetracer.model.Observation
import org.privatetracer.model.toHex

class ScanResultParser : IObservationParser<ScanResult> {
    override fun parse(value: ScanResult, epochInfo: EpochInfo): Observation? {
        val id: ByteArray? = value.scanRecord?.serviceData?.get(ParcelUuid.fromString(ID_UUID))
        if (id == null) {
            Log.w(
                "SCAN",
                "Signal came through filter but could not be parsed: " + value.scanRecord?.bytes?.toHex()
            )
            return null
        }

        val timestamp =
            System.currentTimeMillis() - SystemClock.elapsedRealtime() + (value.timestampNanos / 1000)
        val hashedId = Dp3t.generateHashedId(id, epochInfo.id)

        return Observation(hashedId, timestamp, value.rssi)
    }
}