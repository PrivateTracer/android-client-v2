package org.privatetracer.parser

import org.privatetracer.dp3t.EpochInfo
import org.privatetracer.model.Observation

interface IObservationParser<T> {
    /**
     * Attempts to parse a received value into an observation. Returns null if parsing is unsuccessful.
     * Reasons for failed parsing are possible:
     * 1. Bluetooth scanner is picking up other devices (make the filters more specific)
     * 2. Broadcasting devices are not including the correct data (or using the wrong parcel UUID's)
     */
    fun parse(value: T, epochInfo: EpochInfo): Observation?
}