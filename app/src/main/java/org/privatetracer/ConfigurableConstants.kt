package org.privatetracer

/**
 * TODO (#30/#49): make these configurable (downloadable from the server)
 * This isn't established yet: https://gitlab.com/PrivateTracer/server.azure/-/issues/19
 * Also not sure if all of the values below will eventually be configurable though
 */
class ConfigurableConstants {
    companion object {
        const val CONTACT_DURATION_SECONDS = 180 // TODO: is this already used anywhere? Where should it be implemented?
        const val QUARANTINE_DAYS = 14
        const val EPH_ID_PERSIST_DAYS = 14
        const val CONTACTS_PERSIST_DAYS = 14
        const val REFRESH_TRACING_SETS_MINUTES = 60L
    }
}
