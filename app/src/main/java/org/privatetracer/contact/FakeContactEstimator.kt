package org.privatetracer.contact

import org.privatetracer.model.Observation

class FakeContactEstimator : ContactEstimator() {
    override fun isContact(data: Iterable<Observation>): Boolean {
        return true
    }
}