package org.privatetracer.contact

import android.bluetooth.le.ScanResult
import android.os.Handler
import android.util.Log
import org.privatetracer.bluetooth.GattSpinner
import org.privatetracer.bluetooth.IBluetoothAdvertiser
import org.privatetracer.bluetooth.IBluetoothScanner
import org.privatetracer.dp3t.Dp3t
import org.privatetracer.dp3t.EpochInfo
import org.privatetracer.model.Observation
import org.privatetracer.storage.model.TracerKey
import org.privatetracer.model.toByteArray
import org.privatetracer.model.toHex
import org.privatetracer.parser.IObservationParser
import org.privatetracer.storage.EpochDayCalculator.Companion.currentEpochDay
import java.util.concurrent.TimeUnit

class ContactTracingService(
    private val identityAdvertiser: IBluetoothAdvertiser,
    private val contactScanner: IBluetoothScanner<ScanResult>,
    private val contactManager: ContactManager,
    private val tracerKeyDao: TracerKeyDao,
    private val gattSpinner: GattSpinner,
    private val parser: IObservationParser<ScanResult>
) : AutoCloseable {

    private lateinit var epochInfo: EpochInfo
    private lateinit var seed: ByteArray
    private val epochMoveHandler: Handler = Handler()

    fun start() {
        moveToNewEpoch()

        // TODO: advertise iBeacon for iPhone guys
        contactScanner.scan { sc ->

            // TODO: store observation for later usage
            val observation = parser.parse(sc, epochInfo)
            if (observation == null) {
                Log.w("SCAN", "Could not parse received result, trying iOS")
                // TODO: spin of into ios thread here
                gattSpinner.createJobs(sc.device) {
                    Log.d("SCAN", "We got ${it.toString()} from iOS via callback")
                    val timeStamp = System.currentTimeMillis();
                    val id = it.toByteArray()
                    val hashedId = Dp3t.generateHashedId(id, epochInfo.id)
                    addObservation(Observation(hashedId, timeStamp, sc.rssi))
                }.execute()

                return@scan
            }

            addObservation(observation)

        }
    }

    private fun addObservation(observation: Observation) {
        contactManager.addObservation(observation)

        Log.i(
            "SCAN",
            "Observed: " + observation.beacon.toHex() + " rssi: " + observation.signalStrength
        )
    }

    override fun close() {
        identityAdvertiser.stopBroadcasting()
        contactScanner.stopScanning()
        epochMoveHandler.removeCallbacksAndMessages(null)
    }

    private fun moveToNewEpoch() {
        epochInfo = EpochInfo()
        val key = tracerKeyDao.getTracerKey(epochInfo.id)
        if (key == null) {
            seed = Dp3t.generateSeed()
            tracerKeyDao.addTracerKey(TracerKey(epochInfo.id, seed, currentEpochDay()))
        } else {
            seed = key.seed
        }

        identityAdvertiser.broadcast(Dp3t.generateEphId(seed))
        contactManager.onMovedToNewEpoch()
        val delay = (epochInfo.nextAfter * 1000) - System.currentTimeMillis()
        Log.d(
            "CONTACT",
            "Next iteration in: " + TimeUnit.MILLISECONDS.toMinutes(delay) + " minutes"
        )

        epochMoveHandler.postDelayed(this::moveToNewEpoch, delay)
    }

    fun isScanning() = contactScanner.isScanning
}
