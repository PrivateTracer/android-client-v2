package org.privatetracer.contact

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import org.privatetracer.storage.model.Contact

@Dao
interface ContactDao {

    @Insert(onConflict = REPLACE)
    fun addContact(contact: Contact)

    @Insert(onConflict = REPLACE)
    fun addContacts(contacts: Iterable<Contact>)

    @Query("SELECT * FROM contact")
    suspend fun getContacts(): List<Contact>

    @Query("UPDATE contact SET was_infected = 1 WHERE beacon == :beacon")
    fun setInfected(beacon: ByteArray)

    @Query("DELETE FROM contact WHERE day < :epochDay")
    fun removeContactsBefore(epochDay: Long)
}
