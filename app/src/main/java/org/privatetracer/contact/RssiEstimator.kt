package org.privatetracer.contact

import org.privatetracer.model.Observation

const val THRESHOLD = -68

class RssiEstimator : ContactEstimator() {
    override fun isContact(data: Iterable<Observation>): Boolean {
        return data.filter { it.signalStrength > THRESHOLD }
            .count() > 0
    }
}