package org.privatetracer.contact

import org.privatetracer.dp3t.EpochInfo
import org.privatetracer.storage.model.Contact
import org.privatetracer.model.Observation
import org.privatetracer.model.hexStringToByteArray
import org.privatetracer.storage.EpochDayCalculator.Companion.currentEpochDay

abstract class ContactEstimator {
    /**
     * Decide whether given observations provide enough reason to say there was contact.
     * This can depend on duration and signal strength.
     */
    abstract fun isContact(data: Iterable<Observation>): Boolean

    private fun toContact(beacon: String, data: Iterable<Observation>): Contact {
        return Contact(
            beacon = beacon.hexStringToByteArray(),
            day = currentEpochDay(),
            epochId = EpochInfo().id.toLong()
        )
    }

    fun toContacts(data: Map<String, Iterable<Observation>>): Iterable<Contact> {
        return data.entries.filter { isContact(it.value) }
            .map { toContact(it.key, it.value) }
    }
}
