package org.privatetracer.contact

import android.content.Context
import org.privatetracer.model.Observation
import org.privatetracer.storage.PrivateTracerPreferences

class BarkModeEstimator(val context: Context) : ContactEstimator() {

    companion object {

        const val MIN_RSSI = -127
        const val MAX_RSSI = -40
        const val RSSI_2M = -60.433

        /**
         * according to the docs, the range is [-127,126] but in practise it's never above 0
         * [https://developer.android.com/reference/android/bluetooth/le/ScanResult#getRssi()]
         **/
        fun calcThresholdFromSensitivity(sensitivity: Int): Int {
            return remapInt(sensitivity, 0, 100, MAX_RSSI, MIN_RSSI)
        }

        fun estimateDistanceFromDbm(dBm: Int): Double {
            val estimation = remapDouble(dBm.toDouble(), MAX_RSSI.toDouble(), RSSI_2M, 0.0, 2.0)
            return if (estimation < 0) {
                0.0
            } else {
                estimation
            }
        }

        fun calcSensitivityFromThreshold(threshold: Int = RSSI_2M.toInt()): Int {
            return remapInt(
                threshold,
                MAX_RSSI,
                MIN_RSSI,
                0,
                100
            )
        }

        /**
         * Re-map to different number range
         * See: [https://www.arduino.cc/reference/en/language/functions/math/map/]
         */
        @JvmStatic
        fun remapInt(x: Int, in_min: Int, in_max: Int, out_min: Int, out_max: Int): Int {
            return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min
        }

        /**
         * Re-map to different number range
         * See: ]https://www.arduino.cc/reference/en/language/functions/math/map/]
         */
        @JvmStatic
        fun remapDouble(
            x: Double,
            in_min: Double,
            in_max: Double,
            out_min: Double,
            out_max: Double
        ): Double {
            return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min
        }
    }

    private val prefs = PrivateTracerPreferences(context)

    override fun isContact(data: Iterable<Observation>): Boolean {
        if (!prefs.isBarkModeEnabled()) return false
        val sensitivity = prefs.getBarkModeSensitivity()
        return data.filter { it.signalStrength > calcThresholdFromSensitivity(sensitivity) }
            .count() > 0
    }
}
