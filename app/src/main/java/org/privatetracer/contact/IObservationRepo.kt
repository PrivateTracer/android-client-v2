package org.privatetracer.contact

import org.privatetracer.model.Observation

interface IObservationRepo {
    /**
     * Observations are managed for the duration of an epoch. After the epoch,
     * they can be cleared since all beacons will change their ID.
     */
    fun addObservation(observation: Observation)

    fun getObservations(): Map<String, Collection<Observation>>
    fun clearObservations()
}
