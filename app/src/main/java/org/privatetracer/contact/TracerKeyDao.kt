package org.privatetracer.contact

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import org.privatetracer.storage.model.TracerKey

@Dao
interface TracerKeyDao {
    /**
     * Adds a new tracer key. Epoch day is used for easier truncation later one.
     */
    @Insert
    fun addTracerKey(tracerKey: TracerKey)

    @Query("SELECT * FROM tracerkey WHERE epoch == :epoch LIMIT 1")
    fun getTracerKey(epoch: Int): TracerKey?

    /**
     * Fetches all tracer keys
     */
    @Query("SELECT * FROM tracerkey")
    fun getTracerKeys(): List<TracerKey>

    /**
     * Remove all keys that are before the given epoch day
     */
    @Query("DELETE FROM tracerkey WHERE epoch_day < :epochDay")
    fun removeOldTracerKeysBefore(epochDay: Long)
}
