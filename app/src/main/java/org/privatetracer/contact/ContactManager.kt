package org.privatetracer.contact

import android.os.VibrationEffect
import android.os.Vibrator
import android.util.Log
import org.privatetracer.ConfigurableConstants.Companion.QUARANTINE_DAYS
import org.privatetracer.dp3t.CuckooFilter
import org.privatetracer.model.Observation
import org.privatetracer.storage.IDataRetention
import org.privatetracer.storage.IEpochDayCalculator

class ContactManager private constructor(
    private val contactRepo: ContactDao,
    private val observationRepo: IObservationRepo,
    private val dataRetentionManager: IDataRetention,
    private val epochDayCalculator: IEpochDayCalculator,
    private val contactEstimator: ContactEstimator,
    private val barkModeEstimator: ContactEstimator? = null,
    private val vibrator: Vibrator? = null
) {
    companion object {
        fun initialize(
            contactRepo: ContactDao,
            observationRepo: IObservationRepo,
            dataRetentionManager: IDataRetention,
            epochDayCalculator: IEpochDayCalculator,
            contactEstimator: ContactEstimator,
            barkModeEstimator: ContactEstimator? = null,
            vibrator: Vibrator? = null
        ) {
            m_instance = ContactManager(
                contactRepo,
                observationRepo,
                dataRetentionManager,
                epochDayCalculator,
                contactEstimator,
                barkModeEstimator,
                vibrator
            )
        }

        private lateinit var m_instance: ContactManager

        val instance: ContactManager
            get() = m_instance
    }

    /**
     * Add an observation of a beacon
     */
    fun addObservation(observation: Observation) {
        observationRepo.addObservation(observation)
        //Check for BarkMode
        barkModeEstimator?.let { estimator ->
            vibrator?.let { vib ->
                if (!vib.hasVibrator()) return
                val shouldBark = estimator.isContact(listOf(observation))
                if (shouldBark) {
                    if (android.os.Build.VERSION.SDK_INT >= 26) {
                        val vibEffect =
                            VibrationEffect.createOneShot(250, VibrationEffect.DEFAULT_AMPLITUDE)
                        vib.vibrate(vibEffect)
                    } else {
                        @Suppress("DEPRECATION")
                        //yes deprecated, but  we need to support older devices.
                        vib.vibrate(250)
                    }
                } else {
                    //vib.cancel()
                }
            }
        }
    }

    /**
     * Should be called when we pass into a new epoch. All received observations are transformed to
     * contacts where applicable using provided ContactEstimator.
     */
    fun onMovedToNewEpoch() {
        processObservations()
        dataRetentionManager.validateDataRetention()
    }

    /**
     * Upon receiving possible infection, check whether we can find a derivative beacon
     */
    suspend fun updateInfectedContacts(json: String) {
        val cf: CuckooFilter
        try {
            cf = CuckooFilter.deserialize_blob(json)
        } catch (up: Throwable) {
            Log.e("CONTACT", "Could not parse cookoo filter: $json")
            throw up
        }

        for (contact in contactRepo.getContacts()) {
            if (cf.contains(contact.beacon)) {
                Log.d("CONTACT", "Infection found for contact with id: ${contact.contactId}")
                contactRepo.setInfected(contact.beacon)
            }
        }
    }

    suspend fun getDaysAtRisk(): Int {
        val decay = QUARANTINE_DAYS
        val infectedContacts = contactRepo.getContacts().filter { it.wasInfected }
        val lastInfectedContactDay = infectedContacts.maxBy { it.day }?.day ?: 0
        val today = epochDayCalculator.getCurrentEpochDay()
        var quarantineDaysLeft = (decay - (today - lastInfectedContactDay))
        if (quarantineDaysLeft < 0) {
            quarantineDaysLeft = 0
        }
        return quarantineDaysLeft.toInt()
    }

    private fun processObservations() {
        Log.i("CONTACT", "Processing observations")

        val data = observationRepo.getObservations()
        val contacts = contactEstimator.toContacts(data)
        contactRepo.addContacts(contacts)
        observationRepo.clearObservations()

        Log.i("CONTACT", "Added " + contacts.count() + " new contacts")
    }
}
