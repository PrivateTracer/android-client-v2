package org.privatetracer.contact

import androidx.room.*
import org.privatetracer.storage.model.TracingSetId

@Dao
interface TracingSetIdsDao {

    @Query("SELECT * FROM tracingsetid")
    suspend fun getAll(): List<TracingSetId>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addTracingSetId(tracingSetId: TracingSetId)

    @Delete
    suspend fun deleteTracingSetId(tracingSetId: TracingSetId)
}
