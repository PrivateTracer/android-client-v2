package org.privatetracer.contact

import org.privatetracer.model.Observation
import org.privatetracer.model.toHex

object MemoryContactRepo : IObservationRepo {
    private val data: MutableMap<String, MutableCollection<Observation>> = hashMapOf()

    override fun addObservation(observation: Observation) {
        data.getOrPut(observation.beacon.toHex(), { mutableListOf() }).add(observation)
    }

    override fun getObservations(): Map<String, Collection<Observation>> {
        return data
    }

    override fun clearObservations() {
        data.clear()
    }
}