package org.privatetracer.storage

import androidx.room.TypeConverter
import org.privatetracer.model.hexStringToByteArray
import org.privatetracer.model.toHex

class Converters {
    @TypeConverter
    fun fromByteArray(value: ByteArray?): String? {
        return value?.toHex()
    }

    @TypeConverter
    fun fromHexString(value: String?): ByteArray? {
        return value?.hexStringToByteArray()
    }
}