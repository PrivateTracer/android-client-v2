package org.privatetracer.storage

import android.app.Service
import android.content.Context
import android.util.Log
import org.privatetracer.ConfigurableConstants.Companion.CONTACTS_PERSIST_DAYS
import org.privatetracer.ConfigurableConstants.Companion.EPH_ID_PERSIST_DAYS
import androidx.work.*
import androidx.work.PeriodicWorkRequest.MIN_PERIODIC_INTERVAL_MILLIS
import org.privatetracer.BuildConfig
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeUnit.DAYS
import java.util.concurrent.TimeUnit.MILLISECONDS

private const val TAG = "DataRetentionManager"

interface IDataRetention {
    fun validateDataRetention()
}

class DataRetentionManager(
    private val prefs: PrivateTracerPreferences,
    private val epochDayCalculator: IEpochDayCalculator = EpochDayCalculator()
) : IDataRetention {

    override fun validateDataRetention() {
        Log.d(TAG, "validateDataRetention")

        val dataLastRemovedEpochDay = prefs.getDataLastRemovedEpochDay()
        val currentEpochDay = epochDayCalculator.getCurrentEpochDay()
        Log.d(TAG, "dataLastRemovedEpochDay = $dataLastRemovedEpochDay & currentEpochDay = $currentEpochDay")

        if (dataLastRemovedEpochDay < currentEpochDay) {
            Log.i(TAG, "remove old data")
            val db = PrivateTracerDatabase.instance

            db.contactDao().removeContactsBefore(currentEpochDay - CONTACTS_PERSIST_DAYS)
            db.tracerKeyDao().removeOldTracerKeysBefore(currentEpochDay - EPH_ID_PERSIST_DAYS)

            prefs.setDataLastRemovedEpochDay(currentEpochDay)
        }
    }

    /**
     * A background worker job to validate data retention (delete old data).
     * This enables the device to spread its memory resources and battery consumption.
     * If this isn't implemented using the [WorkManager] (but eg. a background [Service]), devices running newer
     * Android versions might decide to kill the background process!
     * Read more [here](https://medium.com/@suchibansal/android-workmanager-bfc0fd3dd9a2)
     * */
    class DataRetentionValidator(context: Context, workerParams: WorkerParameters) : Worker(context, workerParams) {
        companion object {
            private const val WORKER_TAG = "DataRetentionValidator"

            private val workManager = WorkManager.getInstance()

            /**
             * Data retention validation is triggered and repeated periodically in the background.
             * The background work can be delayed if the device is busy or idle, but only by a maximum amount of time.
             * If work is already being performed, it is cancelled before a new work is started to prevent a buildup.
             */
            fun start() {
                workManager.cancelAllWorkByTag(WORKER_TAG)
                workManager.enqueue(
                    periodicRequestBuilder
                        .setConstraints(constraints)
                        .addTag(WORKER_TAG)
                        .build()
                )
            }

            private val periodicRequestBuilder =
                if (BuildConfig.DEBUG) {
                    // Use a short periodic interval to easily test the background work on DEBUG.
                    // This is also meant to prevent issues with testing below MIN_PERIODIC_INTERVAL_MILLIS,
                    // which could take a while to notice.
                    PeriodicWorkRequest.Builder(
                        DataRetentionValidator::class.java, MIN_PERIODIC_INTERVAL_MILLIS, MILLISECONDS
                    )
                } else {
                    PeriodicWorkRequest.Builder(
                        DataRetentionValidator::class.java, 1, DAYS
                    )
                }

            private val constraints = Constraints.Builder()
                .setTriggerContentMaxDelay(1, TimeUnit.HOURS)
                .build()
        }

        override fun doWork(): Result {
            DataRetentionManager(PrivateTracerPreferences(applicationContext)).validateDataRetention()
            return Result.success()
        }
    }
}
