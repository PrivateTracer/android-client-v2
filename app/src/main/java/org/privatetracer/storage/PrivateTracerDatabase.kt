package org.privatetracer.storage

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import org.privatetracer.storage.model.TracingSetId
import org.privatetracer.contact.ContactDao
import org.privatetracer.contact.TracerKeyDao
import org.privatetracer.contact.TracingSetIdsDao
import org.privatetracer.storage.model.Contact
import org.privatetracer.storage.model.TracerKey

const val DB_NAME = "private_tracer.db"

@Database(entities = [TracerKey::class, Contact::class, TracingSetId::class], version = 4)
@TypeConverters(Converters::class)
abstract class PrivateTracerDatabase : RoomDatabase() {
    abstract fun tracingSetIdsDao(): TracingSetIdsDao

    abstract fun tracerKeyDao(): TracerKeyDao

    abstract fun contactDao(): ContactDao

    companion object {
        private lateinit var m_instance: PrivateTracerDatabase
        val instance: PrivateTracerDatabase
            get() = m_instance

        fun initialize(context: Context) {
            m_instance = Room
                .databaseBuilder(context, PrivateTracerDatabase::class.java, DB_NAME)
                .allowMainThreadQueries() // TODO don't make calls on the main thread ;)
                .fallbackToDestructiveMigration()
                .build()
        }
    }
}
