package org.privatetracer.storage

import java.util.concurrent.TimeUnit

interface IEpochDayCalculator {
    fun getCurrentEpochDay(): Long
}

class EpochDayCalculator : IEpochDayCalculator {
    companion object {
        fun currentEpochDay(): Long {
            return TimeUnit.MILLISECONDS.toDays(System.currentTimeMillis())
        }
    }

    override fun getCurrentEpochDay(): Long {
        return currentEpochDay()
    }
}
