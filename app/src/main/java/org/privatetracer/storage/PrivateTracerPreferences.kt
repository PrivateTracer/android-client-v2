package org.privatetracer.storage

import android.content.Context
import android.content.SharedPreferences
import androidx.preference.PreferenceManager
import org.privatetracer.contact.BarkModeEstimator
import org.privatetracer.contact.BarkModeEstimator.Companion.calcSensitivityFromThreshold

private const val PREFS_AUTO_MATCH_ENABLED = "PREFS_AUTO_MATCH_ENABLED"
private const val PREFS_STATUS_LAST_REFRESHED = "PREFS_STATUS_LAST_REFRESHED"
private const val PREFS_BARK_MODE_ENABLED = "PREFS_BARK_MODE_ENABLED"
private const val PREFS_BARK_MODE_SENSITIVITY = "PREFS_BARK_MODE_SENSITIVITY"
private const val PREFS_BATTERY_OPTIMIZATION = "PREFS_BATTERY_OPTIMIZATION"
private const val PREFS_DATA_LAST_REMOVED_EPOCH_DAY = "PREFS_DATA_LAST_REMOVED_EPOCH_DAY"

private val BARK_MODE_SENSITIVITY_DEFAULT = calcSensitivityFromThreshold(BarkModeEstimator.RSSI_2M.toInt())

const val NOT_REFRESHED_BEFORE = -1L

class PrivateTracerPreferences(context: Context) {
    private val prefs: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)

    fun isAutoMatchEnabled(): Boolean {
        return prefs.getBoolean(PREFS_AUTO_MATCH_ENABLED, false)
    }

    fun setAutoMatchEnabled(shouldEnable: Boolean) {
        prefs.edit().putBoolean(PREFS_AUTO_MATCH_ENABLED, shouldEnable).apply()
    }

    fun getStatusLastRefreshedMillis(): Long {
        return prefs.getLong(PREFS_STATUS_LAST_REFRESHED, NOT_REFRESHED_BEFORE)
    }

    fun updateStatusLastRefreshed() {
        prefs.edit().putLong(PREFS_STATUS_LAST_REFRESHED, System.currentTimeMillis()).apply()
    }

    fun isBarkModeEnabled(): Boolean {
        return prefs.getBoolean(PREFS_BARK_MODE_ENABLED, false)
    }

    fun setBarkModeEnabled(shouldEnable: Boolean) {
        prefs.edit().putBoolean(PREFS_BARK_MODE_ENABLED, shouldEnable).apply()
    }

    fun getBarkModeSensitivity(): Int {
        return prefs.getInt(PREFS_BARK_MODE_SENSITIVITY, BARK_MODE_SENSITIVITY_DEFAULT)
    }

    fun setBarkModeSensitivity(progress: Int) {
        prefs.edit().putInt(PREFS_BARK_MODE_SENSITIVITY, progress).apply()
    }

    fun isBatteryOptimizationPermissionRequested(): Boolean {
        return prefs.getBoolean(PREFS_BATTERY_OPTIMIZATION, false)
    }

    fun setBatteryOptimizationPermissionRequested(isRequested: Boolean) {
        prefs.edit().putBoolean(PREFS_BATTERY_OPTIMIZATION, isRequested).apply()
    }

    fun getDataLastRemovedEpochDay(): Long {
        return prefs.getLong(PREFS_DATA_LAST_REMOVED_EPOCH_DAY, 0)
    }

    fun setDataLastRemovedEpochDay(currentEpochDay: Long) {
        prefs.edit().putLong(PREFS_DATA_LAST_REMOVED_EPOCH_DAY, currentEpochDay).apply()
    }
}
