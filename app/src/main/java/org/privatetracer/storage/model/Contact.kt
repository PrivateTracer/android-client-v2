package org.privatetracer.storage.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * A period of contact that was observed close and long enough to be considered a period of infectious contact.
 * TODO: should also include a duration of some sort, but I haven't found a class that fits the API version yet.
 */
@Entity
data class Contact(
    @ColumnInfo(name = "beacon")
    val beacon: ByteArray,
    @ColumnInfo(name = "day")
    val day: Long,
    @ColumnInfo(name = "epoch_id")
    val epochId: Long,
    @ColumnInfo(name = "was_infected")
    val wasInfected: Boolean = false
) {
    // TODO: make beacon primary key when we can guarantee we don't save duplicate contacts
    @PrimaryKey(autoGenerate = true)
    var contactId = 0

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Contact

        if (!beacon.contentEquals(other.beacon)) return false
        if (wasInfected != other.wasInfected) return false

        return true
    }

    override fun hashCode(): Int {
        var result = beacon.contentHashCode()
        result = 31 * result + wasInfected.hashCode()
        return result
    }
}
