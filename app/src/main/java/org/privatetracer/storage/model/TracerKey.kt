package org.privatetracer.storage.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class TracerKey(
    @PrimaryKey
    val epoch: Int,
    @ColumnInfo(name = "seed")
    val seed: ByteArray,
    @ColumnInfo(name = "epoch_day")
    val epochDay: Long
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as TracerKey

        if (epoch != other.epoch) return false
        if (!seed.contentEquals(other.seed)) return false
        if (epochDay != other.epochDay) return false

        return true
    }

    override fun hashCode(): Int {
        var result = epoch
        result = 31 * result + seed.contentHashCode()
        result = 31 * result + epochDay.hashCode()
        return result
    }
}
