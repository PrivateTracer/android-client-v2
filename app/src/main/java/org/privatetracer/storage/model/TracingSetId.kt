package org.privatetracer.storage.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class TracingSetId(
    @PrimaryKey
    @ColumnInfo(name = "tracingSetId")
    val id: String = ""
)
