package org.privatetracer.model

data class Observation(
    val beacon: ByteArray,
    val timestamp: Long,
    val signalStrength: Int
)
