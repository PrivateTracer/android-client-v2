package org.privatetracer.model

import java.nio.ByteBuffer
import java.util.*

const val HEX_PREFIX = "0x"

// These custom conversions conserve leading zero.

fun ByteArray.toHex() =
    HEX_PREFIX + this.joinToString(separator = "") { it.toInt().and(0xff).toString(16).padStart(2, '0') }

fun String.hexStringToByteArray(): ByteArray {
    val hexstring = if (this.startsWith(HEX_PREFIX)) this.substring(2) else this

    return ByteArray(hexstring.length / 2) {
        hexstring.substring(it * 2, it * 2 + 2).toInt(16).toByte()
    }
}

fun String.uuidStringToByteArray(): ByteArray {
    val concat = HEX_PREFIX + this.replace("-", "")
    return concat.hexStringToByteArray()
}

fun UUID.toByteArray(): ByteArray {
    return ByteBuffer.wrap(ByteArray(16)).apply {
        putLong(this@toByteArray.mostSignificantBits)
        putLong(this@toByteArray.leastSignificantBits)
    }.array()
}