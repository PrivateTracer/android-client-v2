package org.privatetracer.model

import org.junit.Assert
import org.junit.Test

class HexTest {
    @Test
    fun fromByteArray() {
        val value = byteArrayOf(0x01, 0x02)
        val string = value.toHex()
        Assert.assertEquals("0x0102", string)
    }

    @Test
    fun fromString() {
        val value = "0102"
        val bytes = value.hexStringToByteArray()
        Assert.assertArrayEquals(byteArrayOf(0x01, 0x02), bytes)
    }

    @Test
    fun fromStringWithPrefix() {
        val value = "0x0102"
        val bytes = value.hexStringToByteArray()
        Assert.assertArrayEquals(byteArrayOf(0x01, 0x02), bytes)
    }

    @Test
    fun fromEmptyString() {
        val value = ""
        val bytes = value.hexStringToByteArray()
        Assert.assertArrayEquals(byteArrayOf(), bytes)
    }
}
