package org.privatetracer.model

import org.junit.Assert
import org.junit.Test
import org.privatetracer.contact.BarkModeEstimator

class BarkModeTest {

    @Test
    fun testBarkMode() {
        Assert.assertEquals(
            BarkModeEstimator.MAX_RSSI,
            BarkModeEstimator.calcThresholdFromSensitivity(0)
        )
        Assert.assertEquals(
            BarkModeEstimator.MIN_RSSI,
            BarkModeEstimator.calcThresholdFromSensitivity(100)
        )

        //
        println(calcSensitivityFromThreshold(BarkModeEstimator.RSSI_2M.toInt()))

        //
        println(calcAvg(openTraceValues))
    }

    //Distance values from OpenTrace
    //https://github.com/opentrace-community/opentrace-calibration/blob/master/Device%20Data.csv
    private val openTraceValues = arrayOf(
        -53.84,// APPLE	    iPhone X
        -59.64,// SAMSUNG	Galaxy Note 9
        -69.13,// SAMSUNG	Galaxy S8
        -57.87,// SAMSUNG	Galaxy S10+
        -49.90,// SAMSUNG	Galaxy Note 10+
        -59.06,// APPLE	    iPhone 6
        -70.56,// HUAWEI	Mate 20 Pro
        -61.78,// SAMSUNG	Galaxy A70
        -59.54,// SONY	    Xperia XZ1
        -63.52,// XIAOMI	Mi A1
        -58.41,// GOOGLE	Pixel 3a XL
        -55.72,// XIAOMI	Pocophone F1
        -59.45,// XIAOMI	Mi A2 Lite
        -67.40,// LG	    Nexus 5X
        -66.57,// SAMSUNG	Galaxy A10S
        -57.51,// OPPO	    AX5s
        -66.74,// OPPO	    Realme 5
        -55.33,// XIAOMI	Mi Max 3
        -56.79,// HUAWEI	Nova 5t
        -59.90 // VIVO	    Y19
    )

    private fun calcSensitivityFromThreshold(threshold: Int): Int {
        return BarkModeEstimator.remapInt(
            threshold,
            BarkModeEstimator.MAX_RSSI,
            BarkModeEstimator.MIN_RSSI,
            0,
            100
        )
    }

    private fun calcAvg(values: Array<Double>): Double {
        var total = 0.0
        values.forEach { nextValue -> total += nextValue }
        return total / values.size
    }

}
