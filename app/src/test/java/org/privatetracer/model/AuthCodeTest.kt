package org.privatetracer.model

import org.junit.Test
import org.privatetracer.AuthCodeGenerator

class AuthCodeTest {

    @Test
    fun test() {
        for (i in 1..10) {
            val someCode = AuthCodeGenerator.generateAuthCode()
            assert(AuthCodeGenerator.validateAuthCode(someCode)) { someCode }
        }
    }
}