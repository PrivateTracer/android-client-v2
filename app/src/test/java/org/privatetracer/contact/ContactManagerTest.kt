package org.privatetracer.contact

import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import org.privatetracer.ConfigurableConstants.Companion.QUARANTINE_DAYS
import org.privatetracer.storage.model.Contact
import org.privatetracer.storage.IDataRetention
import org.privatetracer.storage.IEpochDayCalculator

class ContactManagerTest {
    private lateinit var contactDao: ContactDao
    private lateinit var observationRepo: IObservationRepo
    private lateinit var dataRetention: IDataRetention
    private lateinit var epochDayCalculator: IEpochDayCalculator
    private lateinit var contactEstimator: ContactEstimator
    private lateinit var contactManager: ContactManager

    @Before
    fun setup() {
        contactDao = mock(ContactDao::class.java)
        observationRepo = mock(IObservationRepo::class.java)
        dataRetention = mock(IDataRetention::class.java)
        epochDayCalculator = mock(IEpochDayCalculator::class.java)
        contactEstimator = mock(ContactEstimator::class.java)
        ContactManager.initialize(contactDao, observationRepo, dataRetention, epochDayCalculator, contactEstimator)
        contactManager = ContactManager.instance
    }

    @Test
    fun testGetDaysAtRisk_singleInfected() = runBlocking {
        val today = mockCurrentEpochDay()
        val contact1 = contact(day = today - 5, wasInfected = true)
        val contacts = listOf(contact1)
        `when`(contactDao.getContacts()).thenReturn(contacts)

        val result = contactManager.getDaysAtRisk()

        assertDaysAtRisk(5, result)
    }

    @Test
    fun testGetDaysAtRisk_tooLongAgo() = runBlocking {
        val today = mockCurrentEpochDay()
        val contact1 = contact(day = today - 20, wasInfected = true)
        val contacts = listOf(contact1)
        `when`(contactDao.getContacts()).thenReturn(contacts)

        val result = contactManager.getDaysAtRisk()

        assertInfectionRateNotAtRisk(result)
    }

    @Test
    fun testGetDaysAtRisk_severalContacts() = runBlocking {
        val today = mockCurrentEpochDay()
        val contact4 = contact(day = today - 5, wasInfected = true) // latest infected
        val contact3 = contact(day = today - 10, wasInfected = false)
        val contact2 = contact(day = today - 10, wasInfected = true)
        val contact1 = contact(day = today - 20, wasInfected = true)
        val contacts = listOf(contact1, contact2, contact3, contact4)
        `when`(contactDao.getContacts()).thenReturn(contacts)

        val result = contactManager.getDaysAtRisk()

        assertDaysAtRisk(5, result)
    }

    @Test
    fun testGetDaysAtRisk_latestContactNotInfected() = runBlocking {
        val today = mockCurrentEpochDay()
        val contact4 = contact(day = today - 5, wasInfected = false)
        val contact3 = contact(day = today - 10, wasInfected = true) // latest infected
        val contact2 = contact(day = today - 10, wasInfected = true)
        val contact1 = contact(day = today - 20, wasInfected = true)
        val contacts = listOf(contact1, contact2, contact3, contact4)
        `when`(contactDao.getContacts()).thenReturn(contacts)

        val result = contactManager.getDaysAtRisk()

        assertDaysAtRisk(10, result)
    }

    @Test
    fun testGetDaysAtRisk_noInfected() = runBlocking {
        val today = mockCurrentEpochDay()
        val contact1 = contact(day = today - 5, wasInfected = false)
        val contact2 = contact(day = today - 10, wasInfected = false)
        val contacts = listOf(contact1, contact2)
        `when`(contactDao.getContacts()).thenReturn(contacts)

        val result = contactManager.getDaysAtRisk()

        assertInfectionRateNotAtRisk(result)
    }

    /**
     * @param currentEpochDay mocks the current epoch day as the supplied value.
     * If no value was supplied, then use a default value (this is sufficient for relative calculations)
     * @return currentEpochDay will be returned, so it can be used to make calculations relative to this day.
     */
    private fun mockCurrentEpochDay(currentEpochDay: Long = 100L): Long {
        `when`(epochDayCalculator.getCurrentEpochDay()).thenReturn(currentEpochDay)
        return currentEpochDay
    }

    /** Create a contact to test with */
    private fun contact(
        beacon: ByteArray = ByteArray(0),
        day: Long = 0L,
        epochId: Long = 0L,
        wasInfected: Boolean = false
    ) = Contact(beacon, day, epochId, wasInfected)

    private fun assertInfectionRateNotAtRisk(result: Int) {
        assertEquals(0, result)
    }

    /** Let test result depend on the QUARANTINE_DAYS constant and
     * the number of days you ago you've been in contact with the last infected person as shown by `contactDaysAgo` */
    private fun assertDaysAtRisk(contactDaysAgo: Int, actualValue: Int) {
        val expectedValue = QUARANTINE_DAYS - contactDaysAgo
        assertEquals(
            "Contact was $contactDaysAgo days ago. Expected $expectedValue infectionRate, but was $actualValue",
            expectedValue,
            actualValue
        )
    }
}
