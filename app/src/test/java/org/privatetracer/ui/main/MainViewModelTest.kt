package org.privatetracer.ui.main

import com.nhaarman.mockitokotlin2.argumentCaptor
import com.nhaarman.mockitokotlin2.times
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.never
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner
import org.privatetracer.storage.model.TracingSetId
import org.privatetracer.contact.TracingSetIdsDao
import org.privatetracer.view.main.MainViewModel

@RunWith(MockitoJUnitRunner::class)
class MainViewModelTest {

    private val tracingSetCaptor = argumentCaptor<TracingSetId>()

    @Mock
    lateinit var mockedTracingSetIdsDao: TracingSetIdsDao

    private lateinit var mainViewModel: MainViewModel

    @Before
    fun setUp() {
        mainViewModel = MainViewModel(mockedTracingSetIdsDao)
    }

    @Test
    fun testGetNewestTracingSetIds_noNewTracingSetIds() {
        val newTracingSetIds = emptyList<String>()
        val localTracingSetIds = listOfTracingSetIds("A", "B")

        val newestTracingSetIds = mainViewModel.getNewestTracingSetIds(newTracingSetIds, localTracingSetIds)

        val expected = emptyList<TracingSetId>()
        assertEquals(expected, newestTracingSetIds)
    }

    @Test
    fun testGetNewestTracingSetIds_noLocalTracingSetIds() {
        val newTracingSetIds = listOf("A")
        val localTracingSetIds = listOfTracingSetIds()

        val newestTracingSetIds = mainViewModel.getNewestTracingSetIds(newTracingSetIds, localTracingSetIds)

        val expected = listOfTracingSetIds("A")
        assertEquals(expected, newestTracingSetIds)
    }

    @Test
    fun testGetNewestTracingSetIds_noNewestAndNoLocalTracingSetIds() {
        val newTracingSetIds = emptyList<String>()
        val localTracingSetIds = listOfTracingSetIds()

        val newestTracingSetIds = mainViewModel.getNewestTracingSetIds(newTracingSetIds, localTracingSetIds)

        val expected = emptyList<TracingSetId>()
        assertEquals(expected, newestTracingSetIds)
    }

    @Test
    fun testGetNewestTracingSetIds_singleNewest() {
        val newTracingSetIds = listOf("A", "B")
        val localTracingSetIds = listOfTracingSetIds("A")

        val newestTracingSetIds = mainViewModel.getNewestTracingSetIds(newTracingSetIds, localTracingSetIds)

        val expected = listOfTracingSetIds("B")
        assertEquals(expected, newestTracingSetIds)
    }

    @Test
    fun testGetNewestTracingSetIds_noNewest() {
        val newTracingSetIds = listOf("A", "B")
        val localTracingSetIds = listOfTracingSetIds("A", "B")

        val newestTracingSetIds = mainViewModel.getNewestTracingSetIds(newTracingSetIds, localTracingSetIds)

        val expected = emptyList<TracingSetId>()
        assertEquals(expected, newestTracingSetIds)
    }

    @Test
    fun testGetNewestTracingSetIds_singleNewestAndSingleOldest() {
        val newTracingSetIds = listOf("B", "C")
        val localTracingSetIds = listOfTracingSetIds("A", "B")

        val newestTracingSetIds = mainViewModel.getNewestTracingSetIds(newTracingSetIds, localTracingSetIds)

        val expected = listOfTracingSetIds("C")
        assertEquals(expected, newestTracingSetIds)
    }

    @Test
    fun testGetNewestTracingSetIds_noNewestAndSingleOldest() {
        val newTracingSetIds = listOf("B")
        val localTracingSetIds = listOfTracingSetIds("A", "B")

        val newestTracingSetIds = mainViewModel.getNewestTracingSetIds(newTracingSetIds, localTracingSetIds)

        val expected = emptyList<TracingSetId>()
        assertEquals(expected, newestTracingSetIds)
    }

    @Test
    fun testGetNewestTracingSetIds_multipleNewestAndNoOldest() {
        val newTracingSetIds = listOf("A", "B", "C")
        val localTracingSetIds = listOfTracingSetIds("A")

        val newestTracingSetIds = mainViewModel.getNewestTracingSetIds(newTracingSetIds, localTracingSetIds)

        val expected = listOfTracingSetIds("B", "C")
        assertEquals(expected, newestTracingSetIds)
    }

    @Test
    fun testGetNewestTracingSetIds_singleNewestAndMultipleOldest() {
        val newTracingSetIds = listOf("C")
        val localTracingSetIds = listOfTracingSetIds("A", "B")

        val newestTracingSetIds = mainViewModel.getNewestTracingSetIds(newTracingSetIds, localTracingSetIds)

        val expected = listOfTracingSetIds("C")
        assertEquals(expected, newestTracingSetIds)
    }

    @Test
    fun testDeleteOldestTracingSetIds_noNewTracingSetIds() = runBlocking {
        val newTracingSetIds = emptyList<String>()
        val localTracingSetIds = listOfTracingSetIds("A", "B")

        mainViewModel.deleteOldestTracingSetIds(newTracingSetIds, localTracingSetIds)

        assertIdsDeleted("A", "B")
    }

    @Test
    fun testDeleteOldestTracingSetIds_noLocalTracingSetIds() = runBlocking {
        val newTracingSetIds = listOf("A")
        val localTracingSetIds = listOfTracingSetIds()

        mainViewModel.deleteOldestTracingSetIds(newTracingSetIds, localTracingSetIds)

        assertNoIdsDeleted()
    }

    @Test
    fun testDeleteOldestTracingSetIds_noNewestAndNoLocalTracingSetIds() = runBlocking {
        val newTracingSetIds = emptyList<String>()
        val localTracingSetIds = listOfTracingSetIds()

        mainViewModel.deleteOldestTracingSetIds(newTracingSetIds, localTracingSetIds)

        assertNoIdsDeleted()
    }

    @Test
    fun testDeleteOldestTracingSetIds_singleNewest() = runBlocking {
        val newTracingSetIds = listOf("A", "B")
        val localTracingSetIds = listOfTracingSetIds("A")

        mainViewModel.deleteOldestTracingSetIds(newTracingSetIds, localTracingSetIds)

        assertNoIdsDeleted()
    }

    @Test
    fun testDeleteOldestTracingSetIds_noNewest() = runBlocking {
        val newTracingSetIds = listOf("A", "B")
        val localTracingSetIds = listOfTracingSetIds("A", "B")

        mainViewModel.deleteOldestTracingSetIds(newTracingSetIds, localTracingSetIds)

        assertNoIdsDeleted()
    }

    @Test
    fun testDeleteOldestTracingSetIds_singleNewestAndSingleOldest() = runBlocking {
        val newTracingSetIds = listOf("B", "C")
        val localTracingSetIds = listOfTracingSetIds("A", "B")

        mainViewModel.deleteOldestTracingSetIds(newTracingSetIds, localTracingSetIds)

        assertIdsDeleted("A")
    }

    @Test
    fun testDeleteOldestTracingSetIds_noNewestAndSingleOldest() = runBlocking {
        val newTracingSetIds = listOf("B")
        val localTracingSetIds = listOfTracingSetIds("A", "B")

        mainViewModel.deleteOldestTracingSetIds(newTracingSetIds, localTracingSetIds)

        assertIdsDeleted("A")
    }

    @Test
    fun testDeleteOldestTracingSetIds_multipleNewestAndNoOldest() = runBlocking {
        val newTracingSetIds = listOf("A", "B", "C")
        val localTracingSetIds = listOfTracingSetIds("A")

        mainViewModel.deleteOldestTracingSetIds(newTracingSetIds, localTracingSetIds)

        assertNoIdsDeleted()
    }

    @Test
    fun testDeleteOldestTracingSetIds_singleNewestAndMultipleOldest() = runBlocking {
        val newTracingSetIds = listOf("C")
        val localTracingSetIds = listOfTracingSetIds("A", "B")

        mainViewModel.deleteOldestTracingSetIds(newTracingSetIds, localTracingSetIds)

        assertIdsDeleted("A", "B")
    }

    private fun listOfTracingSetIds(vararg ids: String) = ids.map { TracingSetId(it) }

    private suspend fun assertNoIdsDeleted() {
        verify(mockedTracingSetIdsDao, never()).deleteTracingSetId(tracingSetCaptor.capture())
    }

    private suspend fun assertIdsDeleted(vararg ids: String) {
        verify(mockedTracingSetIdsDao, times(ids.size)).deleteTracingSetId(tracingSetCaptor.capture())
        ids.forEachIndexed { i, id ->
            assertEquals(TracingSetId(id), tracingSetCaptor.allValues[i])
        }
    }
}
